import { Component, OnInit } from '@angular/core';
import {MetierService} from '../service/metier.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Metier} from '../model/metier';
import {SousSecteurService} from '../service/sous-secteur.service';
import {SousSecteur} from '../model/sous-secteur';

@Component({
  selector: 'app-metier-list',
  templateUrl: './metier-list.component.html',
  styleUrls: ['./metier-list.component.scss']
})
export class MetierListComponent implements OnInit {

  /*Cet objet une fois rempli, va être affiché sous forme de liste par le html du component*/
  metiers: Metier[];
  sectCode;
  sss: SousSecteur[];

  constructor(private metierService : MetierService
    ,private route: ActivatedRoute, private _router: Router) {
  }

  ngOnInit() {
    this.sectCode = this.route.snapshot.queryParams['SS'];

    /*On utilise "subscribe" car les sous-secteurs sont soumis à divers changement, et on veut pouvoir notifier les objets ("sousSecteurs") qui ont besoin de savoir ce qui a changé*/
    this.metierService.findAll().subscribe(data => {
      this.metiers = data;
    });
  }

  onMetierClick(code : String){
    console.log("On a cliqué sur le métier de code : "+code);
    /*On navig vers le component ayant pour rôle l'affichage des sous-secteurs + on mets le code du secteur en param*/

    this._router.navigate(["/formations"], {
      relativeTo: this.route,
      queryParams: {
        codeMetier: code
      },
      queryParamsHandling: "merge"
    });
  }


}
