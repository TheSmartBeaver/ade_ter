import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ComponentRoutingService} from '../service/component-routing.service';

@Component({
  selector: 'app-routing',
  templateUrl: './routing.component.html',
  styleUrls: ['./routing.component.scss']
})
export class RoutingComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private router: Router,
              private componentRoutingService: ComponentRoutingService) { }

  ngOnInit(): void {
    this.componentRoutingService.routeToRightComponent();
  }

}
