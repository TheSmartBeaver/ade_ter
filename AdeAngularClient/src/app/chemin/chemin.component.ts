import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SecteurService} from '../service/secteur-service.service';
import {HttpClient, HttpParams} from '@angular/common/http';

@Component({
  selector: 'app-chemin',
  templateUrl: './chemin.component.html',
  styleUrls: ['./chemin.component.scss']
})
export class CheminComponent implements OnInit {


  private secteurNameUrl: string;

  constructor(public route: ActivatedRoute, private http: HttpClient,
              private _router: Router,
              private secteurService: SecteurService) {
    this.secteurNameUrl = 'http://localhost:8095/secteurName';
  }

  isSVisible = false;
  isSSVisible = false;
  isMetierVisible = false;

  sectName;
  sousSectName: string;
  metierName: string;

  DIPName: string;
  DOMName: string;
  COMName: string;
  DESCName: string;

  sectCode: string;
  sousSectCode: string;
  metierCode: string;

  DIPCode: string;
  DOMCode: string;
  COMCode: string;
  DESCCode: string;

  refreshPage(paramToRemove){
    console.log("On a refresh la page");
    let sectCode = this.route.snapshot.queryParams['SECT'];
    let sousSectCode = this.route.snapshot.queryParams['SS'];
    let metierCode = this.route.snapshot.queryParams['codeMetier'];

    let DIPCode = this.route.snapshot.queryParams['DIP'];
    let DOMCode = this.route.snapshot.queryParams['DOM'];
    let COMCode = this.route.snapshot.queryParams['COM'];
    let DESCCode = this.route.snapshot.queryParams['DESC'];

    if(paramToRemove === 'SECT') {
      sectCode = null;
      sousSectCode = null;
    }
    if(paramToRemove === 'SS')
      sousSectCode = null;
    if(paramToRemove === 'codeMetier')
      metierCode = null;

    if(paramToRemove === 'DESC')
      DESCCode = null;
    if(paramToRemove === 'DOM')
      DOMCode = null;
    if(paramToRemove === 'DIP')
      DIPCode = null;
    if(paramToRemove === 'COM')
      COMCode = null;

    this._router.navigate(["/routing"], {
      queryParams: {
        SECT: sectCode,
        SS: sousSectCode,
        codeMetier: metierCode,
        DIP: DIPCode,
        DOM: DOMCode,
        DESC: DESCCode,
        COM: COMCode
      },
      queryParamsHandling: 'merge'
    });
  }

  ngOnInit(): void {

    if(this.sectCode !== 'undefined')
      this.isSVisible = true;

    if(this.sousSectCode !== 'undefined')
      this.isSSVisible = true;

    if(this.metierCode !== 'undefined')
      this.isMetierVisible = true;

    if(this.DIPCode !== 'undefined')
      this.isSVisible = true;

    if(this.DOMCode !== 'undefined')
      this.isSVisible = true;

    if(this.DESCCode !== 'undefined')
      this.isSVisible = true;

    if(this.COMCode !== 'undefined')
      this.isSVisible = true;

    this.findSecteurNameByCode();
    console.log('SECTNAMEEEEE ');
  }

  public findSecteurNameByCode() {
    const sectCode = this.route.snapshot.queryParams['SECT'];
    console.log("WATTTTTTTTTTTTTTTTTTTTT "+sectCode);
    const params = new HttpParams({fromString: "codeS=" + sectCode});
    this.http.get<any>(this.secteurNameUrl, {params}).subscribe(val => {
      this.sectName = val[0];
      console.log("Reçu le nom "+val[0]);
    });
  }

}
