import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

/** Si auto reload marche plus Ubuntu
 * echo "fs.inotify.max_user_watches=524288" | sudo tee -a /etc/sysctl.conf sudo sysctl -p
 */
export class AppComponent implements OnInit {

  title: string;

  isSVisible = false;
  isSSVisible = false;
  isMetierVisible = false;

  isDOMVisible = false;
  isCOMVisible = false;
  isDESCVisible = false;
  isDIPVisible = false;

  sectCode: string;
  sousSectCode: string;
  metierCode: string;

  DIPCode: string;
  DOMCode: string;
  COMCode: string;
  DESCCode: string;

  constructor(public route: ActivatedRoute,
              private _router: Router) {
    this.title = 'Atelier des métiers';
  }

  onCrittClick(){
    this._router.navigate(["/crittSupp"], {
      relativeTo: this.route,
      queryParamsHandling: 'merge'
    });
  }

  refreshPage(paramToRemove){
    console.log("On a refresh la page");
    let sectCode = this.route.snapshot.queryParams['SECT'];
    let sousSectCode = this.route.snapshot.queryParams['SS'];
    let metierCode = this.route.snapshot.queryParams['codeMetier'];

    let DIPCode = this.route.snapshot.queryParams['DIP'];
    let DOMCode = this.route.snapshot.queryParams['DOM'];
    let COMCode = this.route.snapshot.queryParams['COM'];
    let DESCCode = this.route.snapshot.queryParams['DESC'];

    if(paramToRemove === 'SECT') {
      sectCode = null;
      sousSectCode = null;
    }
    if(paramToRemove === 'SS')
      sousSectCode = null;
    if(paramToRemove === 'codeMetier')
      metierCode = null;

    if(paramToRemove === 'DESC')
      DESCCode = null;
    if(paramToRemove === 'DOM')
      DOMCode = null;
    if(paramToRemove === 'DIP')
      DIPCode = null;
    if(paramToRemove === 'COM')
      COMCode = null;

    this._router.navigate(["/routing"], {
      queryParams: {
        SECT: sectCode,
        SS: sousSectCode,
        codeMetier: metierCode,
        DIP: DIPCode,
        DOM: DOMCode,
        DESC: DESCCode,
        COM: COMCode
      },
      queryParamsHandling: 'merge'
    });
  }

  ngOnInit(): void {

    if(this.sectCode !== 'undefined')
      this.isSVisible = true;

    if(this.sousSectCode !== 'undefined')
      this.isSSVisible = true;

    if(this.metierCode !== 'undefined')
      this.isMetierVisible = true;

    if(this.DIPCode !== 'undefined')
      this.isSVisible = true;

    if(this.DOMCode !== 'undefined')
      this.isSVisible = true;

    if(this.DESCCode !== 'undefined')
      this.isSVisible = true;

    if(this.COMCode !== 'undefined')
      this.isSVisible = true;

    console.log('SECTCODEEEEEEEEEEEEEEE ' + this.sectCode);
  }
}
