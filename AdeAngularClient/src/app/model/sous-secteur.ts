import {Metier} from './metier';

export class SousSecteur {
  nom: string;
  code: string;
  metiers: Metier;
}
