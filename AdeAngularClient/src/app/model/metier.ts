import {Formation} from './formation';

export class Metier {
  nom: string;
  code: string;
  formations: Formation;
}
