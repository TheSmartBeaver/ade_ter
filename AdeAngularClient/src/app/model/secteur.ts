import {SousSecteur} from './sous-secteur';

export class Secteur {
  /*On récupère les propriétés des secteurs qui nous intéressent*/

  id: number;
  nom: string;
  // sousSecteurList: any;
  code: string;
  description: string;
  sousSecteurs: SousSecteur;
}
