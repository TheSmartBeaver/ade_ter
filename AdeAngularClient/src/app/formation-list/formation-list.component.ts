import { Component, OnInit } from '@angular/core';
import {Metier} from '../model/metier';
import {MetierService} from '../service/metier.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Formation} from '../model/formation';
import {FormationService} from '../service/formation.service';

@Component({
  selector: 'app-formation-list',
  templateUrl: './formation-list.component.html',
  styleUrls: ['./formation-list.component.scss']
})
export class FormationListComponent implements OnInit {

  /*Cet objet une fois rempli, va être affiché sous forme de liste par le html du component*/
  formations: Formation[];
  metiers: Metier[];

  constructor(private metierService : MetierService
    ,private route: ActivatedRoute, private _router : Router,
              private formationService : FormationService) {
  }

  ngOnInit() {
    const metierCode = this.route.snapshot.queryParams['codeMetier'];

    /*On utilise "subscribe" car les sous-secteurs sont soumis à divers changement, et on veut pouvoir notifier les objets ("sousSecteurs") qui ont besoin de savoir ce qui a changé*/
    this.formationService.findAll().subscribe(data => {
      this.formations = data;
    });
  }

  onClickFormation(code : string){
    this.formationService.gotoURLOfFormation(code);
  }

}
