import { Component, OnInit } from '@angular/core';
import {Secteur} from '../model/secteur';
import {ComposanteService} from '../service/composante.service';
import {FormBuilder, NgForm} from '@angular/forms';
import {TypeDiplome} from '../model/type-diplome';
import {DescRegime} from '../model/desc-regime';
import {Domaine} from '../model/domaine';
import {TypeDiplomeService} from '../service/type-diplome.service';
import {DescRegimeService} from '../service/desc-regime.service';
import {DomaineService} from '../service/domaine.service';
import {Composante} from '../model/composante';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Formation} from '../model/formation';

@Component({
  selector: 'app-critt-supp-list',
  templateUrl: './critt-supp-list.component.html',
  styleUrls: ['./critt-supp-list.component.scss']
})
export class CrittSuppListComponent implements OnInit {

  constructor(private composanteService: ComposanteService,
              private typeDiplomeService: TypeDiplomeService,
              private descRegimeService: DescRegimeService,
              private domaineService: DomaineService,
              private http: HttpClient,
              private route: ActivatedRoute,
              private router: Router) {}

  composantes: Composante[];
  typeDiplomes: TypeDiplome[];
  descRegimes: DescRegime[];
  domaines: Domaine[];

  DIP: string;
  DOM: string;
  DESC: string;
  COM: string;

  ngOnInit(): void {

  }

  onCritereClick(chemin){
    this.router.navigate([chemin], {
      relativeTo: this.route,
      queryParamsHandling: "merge"
    });
  }




}
