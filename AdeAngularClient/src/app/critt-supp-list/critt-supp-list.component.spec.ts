import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrittSuppListComponent } from './critt-supp-list.component';

describe('CrittSuppListComponent', () => {
  let component: CrittSuppListComponent;
  let fixture: ComponentFixture<CrittSuppListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrittSuppListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrittSuppListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
