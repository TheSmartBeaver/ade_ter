import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SecteurListComponent} from './secteur-list/secteur-list.component';
import {MetierListComponent} from './metier-list/metier-list.component';
import {FormationListComponent} from './formation-list/formation-list.component';
import {CrittSuppListComponent} from './critt-supp-list/critt-supp-list.component';
import {RoutingComponent} from './routing/routing.component';
import {CrittDomaineComponent} from './critt-supp/critt-domaine/critt-domaine.component';
import {CrittComposanteComponent} from './critt-supp/critt-composante/critt-composante.component';
import {CrittDescRegimeComponent} from './critt-supp/critt-desc-regime/critt-desc-regime.component';
import {CrittDiplomeComponent} from './critt-supp/critt-diplome/critt-diplome.component';

const routes: Routes = [
  { path: '', redirectTo: 'secteurs', pathMatch: 'full' },
  { path: 'secteurs', component: SecteurListComponent },
  { path: 'metiers', component: MetierListComponent },
  { path: 'formations', component: FormationListComponent },
  { path: 'crittSupp', component: CrittSuppListComponent },
  { path: 'routing', component: RoutingComponent },
  { path: 'domaine', component: CrittDomaineComponent },
  { path: 'composante', component: CrittComposanteComponent },
  { path: 'descRegime', component: CrittDescRegimeComponent },
  { path: 'diplome', component: CrittDiplomeComponent }
];

/*On répertorie toutes nos routes ici*/

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
