import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SousSecteur} from '../model/sous-secteur';
import {Metier} from '../model/metier';
import {Secteur} from '../model/secteur';
import {ActivatedRoute} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MetierService {

  private metiersUrl: string;
  private metiersofCodeUrl: string;

  constructor(private http: HttpClient, private route : ActivatedRoute) {
    this.metiersUrl = 'http://localhost:8095/metiersList';
    this.metiersofCodeUrl = 'http://localhost:8095/metierOfCode';
  }


  public findMetierOfCode(codeMetier : String): Observable<Metier[]> {
    console.log('On demande le métier de code '+codeMetier);
    const params = new HttpParams({fromString: "codeMetier="+codeMetier});
    return this.http.get<Metier[]>(this.metiersofCodeUrl, {params});
  }

  public findAll(): Observable<Metier[]> {
    const sectCode = this.route.snapshot.queryParams['SECT'];
    const sousSectCode = this.route.snapshot.queryParams['SS'];
    const metierCode = this.route.snapshot.queryParams['codeMetier'];

    const DIPCode = this.route.snapshot.queryParams['DIP'];
    const DOMCode = this.route.snapshot.queryParams['DOM'];
    const COMCode = this.route.snapshot.queryParams['COM'];
    const DESCCode = this.route.snapshot.queryParams['DESC'];

    let paramsInString = "";
    paramsInString = paramsInString + "codeMetier="+metierCode+"&";
    paramsInString = paramsInString + "codeSS="+sousSectCode+"&";
    paramsInString = paramsInString + "codeS="+sectCode+"&";

    paramsInString = paramsInString + "codeDIP="+DIPCode+"&";
    paramsInString = paramsInString + "codeDOM="+DOMCode+"&";
    paramsInString = paramsInString + "codeCOM="+COMCode+"&";
    paramsInString = paramsInString + "codeDESC="+DESCCode;

    const params = new HttpParams({fromString: paramsInString});
    console.log('On demande tous les secteurs');
    return this.http.get<Metier[]>(this.metiersUrl, {params});
  }
}
