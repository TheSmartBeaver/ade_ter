import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {DescRegime} from '../model/desc-regime';

@Injectable({
  providedIn: 'root'
})
export class DescRegimeService {

  private descRegimeUrl: string;

  constructor(private http: HttpClient) {
    this.descRegimeUrl = 'http://localhost:8095/descRegimes';
  }

  public findAll(): Observable<DescRegime[]> {
    return this.http.get<DescRegime[]>(this.descRegimeUrl);
  }

  public find(params): Observable<DescRegime[]> {
    return this.http.get<DescRegime[]>(this.descRegimeUrl, {params});
  }
}
