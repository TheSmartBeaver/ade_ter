import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {SousSecteur} from '../model/sous-secteur';

@Injectable({
  providedIn: 'root'
})
export class SousSecteurService {


  private sousSecteursUrl: string;
  private sousSecteurGetByCodeUrl: string;

  constructor(private http: HttpClient) {
    this.sousSecteursUrl = 'http://localhost:8095/sousSecteursList';
    this.sousSecteurGetByCodeUrl = 'http://localhost:8095/ssOfCode';
  }

  public findSSOfSecteur(codeS : String): Observable<SousSecteur[]> {
    console.log('On demande tous les sous-secteurs du secteur de code');
    const params = new HttpParams({fromString: "codeS="+codeS});
    return this.http.get<SousSecteur[]>(this.sousSecteursUrl, {params});
  }

  public findSSOfCode(codeSS : String): Observable<SousSecteur[]> {
    console.log('On demande le sous-secteur de code '+codeSS);
    const params = new HttpParams({fromString: "codeSS="+codeSS});
    return this.http.get<SousSecteur[]>(this.sousSecteurGetByCodeUrl, {params});
  }
}
