import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Secteur } from '../model/secteur';
import { Observable } from 'rxjs/Observable';
import {ActivatedRoute} from '@angular/router';

@Injectable()
export class SecteurService {

  /*Ici on communique avec le back-end, pour récupérer les secteurs du Repository dédié aux secteurs*/

  private secteursUrl: string;

  constructor(private http: HttpClient, private route: ActivatedRoute) {
    this.secteursUrl = 'http://localhost:8095/secteursList'; /*URL vers un REST Controller retournant tous les secteurs existants*/
  }

  public findAll(): Observable<Secteur[]> {
    const sectCode = this.route.snapshot.queryParams['SECT'];
    const sousSectCode = this.route.snapshot.queryParams['SS'];
    const metierCode = this.route.snapshot.queryParams['codeMetier'];

    const DIPCode = this.route.snapshot.queryParams['DIP'];
    const DOMCode = this.route.snapshot.queryParams['DOM'];
    const COMCode = this.route.snapshot.queryParams['COM'];
    const DESCCode = this.route.snapshot.queryParams['DESC'];

    let paramsInString = "";
    paramsInString = paramsInString + "codeMetier="+metierCode+"&";
    paramsInString = paramsInString + "codeSS="+sousSectCode+"&";
    paramsInString = paramsInString + "codeS="+sectCode+"&";

    paramsInString = paramsInString + "codeDIP="+DIPCode+"&";
    paramsInString = paramsInString + "codeDOM="+DOMCode+"&";
    paramsInString = paramsInString + "codeCOM="+COMCode+"&";
    paramsInString = paramsInString + "codeDESC="+DESCCode;

    const params = new HttpParams({fromString: paramsInString});
    console.log('On demande tous les secteurs');
    return this.http.get<Secteur[]>(this.secteursUrl, {params});
  }

}
