import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Formation} from '../model/formation';
import {Composante} from '../model/composante';

@Injectable({
  providedIn: 'root'
})
export class ComposanteService {

  private composantesUrl: string;

  constructor(private http: HttpClient) {
    this.composantesUrl = 'http://localhost:8095/composantes';
  }

  public findAll(): Observable<Composante[]> {
    console.log('On demande toutes les composantes');
    return this.http.get<Composante[]>(this.composantesUrl);
  }

  public find(params): Observable<Composante[]> {
    console.log('On demande toutes les composantes');
    return this.http.get<Composante[]>(this.composantesUrl, {params});
  }
}
