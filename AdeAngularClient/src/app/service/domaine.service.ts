import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {DescRegime} from '../model/desc-regime';
import {Domaine} from '../model/domaine';

@Injectable({
  providedIn: 'root'
})
export class DomaineService {

  private domainesUrl: string;

  constructor(private http: HttpClient) {
    this.domainesUrl = 'http://localhost:8095/domaines';
  }

  public findAll(): Observable<Domaine[]> {
    return this.http.get<Domaine[]>(this.domainesUrl);
  }

  public find(params): Observable<Domaine[]> {
    return this.http.get<Domaine[]>(this.domainesUrl, {params});
  }
}
