import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ComponentRoutingService {

  constructor(private http: HttpClient,
              private route: ActivatedRoute,
              private router: Router) {

  }

  public routeToRightComponent(): void {
    const sectCode = this.route.snapshot.queryParams['SECT'];
    const sousSectCode = this.route.snapshot.queryParams['SS'];
    const metierCode = this.route.snapshot.queryParams['codeMetier'];

    let chemin;

    console.log('sect:'+sectCode+'\n');
    console.log('sousSect:'+sousSectCode+'\n');
    console.log('metier:'+metierCode+'\n');


    if(metierCode!=null && sectCode!=null)
      chemin = ["formations"];
    else if(metierCode!=null && sectCode==null)
      chemin = ["secteurs"];
    else if(sectCode!=null && sousSectCode!=null)
      chemin = ["metiers"];
    else if(sectCode==null && sousSectCode==null && metierCode==null)
      chemin = ["secteurs"];
    else if(sectCode!=null && sousSectCode==null && metierCode==null)
      chemin = ["secteurs"];

    console.log('Le chemin choisit : '+chemin);
    this.router.navigate(chemin, {
      relativeTo: this.route,
      queryParamsHandling: "merge"
    });
  }

}
