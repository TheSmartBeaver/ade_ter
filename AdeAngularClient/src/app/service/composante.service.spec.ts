import { TestBed } from '@angular/core/testing';

import { ComposanteService } from './composante.service';

describe('ComposanteService', () => {
  let service: ComposanteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ComposanteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
