import { TestBed } from '@angular/core/testing';

import { ComponentRoutingService } from './component-routing.service';

describe('ComponentRoutingService', () => {
  let service: ComponentRoutingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ComponentRoutingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
