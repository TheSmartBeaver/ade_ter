import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {DescRegime} from '../model/desc-regime';
import {TypeDiplome} from '../model/type-diplome';

@Injectable({
  providedIn: 'root'
})
export class TypeDiplomeService {

  private typeDiplomesUrl: string;

  constructor(private http: HttpClient) {
    this.typeDiplomesUrl = 'http://localhost:8095/typeDiplomes';
  }

  public findAll(): Observable<TypeDiplome[]> {
    return this.http.get<TypeDiplome[]>(this.typeDiplomesUrl);
  }

  public find(params): Observable<TypeDiplome[]> {
    return this.http.get<TypeDiplome[]>(this.typeDiplomesUrl, {params});
  }
}
