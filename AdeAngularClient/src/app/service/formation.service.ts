import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Metier} from '../model/metier';
import {Formation} from '../model/formation';
import {stringify} from 'querystring';
import {ActivatedRoute, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class FormationService {

  private formationsUrl: string;
  private formationURLUrl: string;
  private receivedURL: string;

  constructor(private http: HttpClient, private _router: Router,
              private route: ActivatedRoute) {
    this.formationsUrl = 'http://localhost:8095/formationsList';
    this.formationURLUrl = 'http://localhost:8095/urlFormation';
    this.receivedURL = ''; /*PAS BON POUR MULTITHREADING ?????*/
  }

  public gotoURLOfFormation(code: string): void {
    const params = new HttpParams({fromString: "codeFormation=" + code});
    this.http.get<any>(this.formationURLUrl, {params}).subscribe(val => {
      this.receivedURL = val[0];
      console.log(val[0]);
      window.open(this.receivedURL, "_blank");
    });
  }

  public findAll(): Observable<Formation[]> {
    const sectCode = this.route.snapshot.queryParams['SECT'];
    const sousSectCode = this.route.snapshot.queryParams['SS'];
    const metierCode = this.route.snapshot.queryParams['codeMetier'];

    const DIPCode = this.route.snapshot.queryParams['DIP'];
    const DOMCode = this.route.snapshot.queryParams['DOM'];
    const COMCode = this.route.snapshot.queryParams['COM'];
    const DESCCode = this.route.snapshot.queryParams['DESC'];


    let paramsInString = "";
    paramsInString = paramsInString + "codeMetier="+metierCode+"&";
    paramsInString = paramsInString + "codeSS="+sousSectCode+"&";
    paramsInString = paramsInString + "codeS="+sectCode+"&";

    paramsInString = paramsInString + "codeDIP="+DIPCode+"&";
    paramsInString = paramsInString + "codeDOM="+DOMCode+"&";
    paramsInString = paramsInString + "codeCOM="+COMCode+"&";
    paramsInString = paramsInString + "codeDESC="+DESCCode;

    const params = new HttpParams({fromString: paramsInString});
    console.log('On demande tous les secteurs');
    return this.http.get<Formation[]>(this.formationsUrl, {params});
  }
}
