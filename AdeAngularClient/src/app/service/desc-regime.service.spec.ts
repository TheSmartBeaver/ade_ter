import { TestBed } from '@angular/core/testing';

import { DescRegimeService } from './desc-regime.service';

describe('DescRegimeService', () => {
  let service: DescRegimeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DescRegimeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
