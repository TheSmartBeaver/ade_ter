import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SecteurListComponent } from './secteur-list/secteur-list.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SecteurService} from './service/secteur-service.service';
import {SousSecteurService} from './service/sous-secteur.service';
import { MetierListComponent } from './metier-list/metier-list.component';
import { FormationListComponent } from './formation-list/formation-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatExpansionModule} from '@angular/material/expansion';
import { CrittSuppListComponent } from './critt-supp-list/critt-supp-list.component';
import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RoutingComponent } from './routing/routing.component';
import { CrittComposanteComponent } from './critt-supp/critt-composante/critt-composante.component';
import { CrittDomaineComponent } from './critt-supp/critt-domaine/critt-domaine.component';
import { CrittDescRegimeComponent } from './critt-supp/critt-desc-regime/critt-desc-regime.component';
import { CrittDiplomeComponent } from './critt-supp/critt-diplome/critt-diplome.component';

/*On déclare tous les "components", tous les modules et services utilisés par ces "components"*/

@NgModule({
  declarations: [
    AppComponent,
    SecteurListComponent,
    MetierListComponent,
    FormationListComponent,
    CrittSuppListComponent,
    RoutingComponent,
    CrittComposanteComponent,
    CrittDomaineComponent,
    CrittDescRegimeComponent,
    CrittDiplomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [SecteurService, SousSecteurService],
  bootstrap: [AppComponent]
})
export class AppModule { }
