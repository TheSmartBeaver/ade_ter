import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrittDiplomeComponent } from './critt-diplome.component';

describe('CrittDiplomeComponent', () => {
  let component: CrittDiplomeComponent;
  let fixture: ComponentFixture<CrittDiplomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrittDiplomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrittDiplomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
