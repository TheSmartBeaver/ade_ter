import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrittComposanteComponent } from './critt-composante.component';

describe('CrittComposanteComponent', () => {
  let component: CrittComposanteComponent;
  let fixture: ComponentFixture<CrittComposanteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrittComposanteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrittComposanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
