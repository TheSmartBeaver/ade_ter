import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrittDomaineComponent } from './critt-domaine.component';

describe('CrittDomaineComponent', () => {
  let component: CrittDomaineComponent;
  let fixture: ComponentFixture<CrittDomaineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrittDomaineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrittDomaineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
