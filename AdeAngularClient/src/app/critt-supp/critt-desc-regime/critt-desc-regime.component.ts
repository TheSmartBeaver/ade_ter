import { Component, OnInit } from '@angular/core';
import {Composante} from '../../model/composante';
import {ComposanteService} from '../../service/composante.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {DescRegimeService} from '../../service/desc-regime.service';
import {DescRegime} from '../../model/desc-regime';

@Component({
  selector: 'app-critt-desc-regime',
  templateUrl: './critt-desc-regime.component.html',
  styleUrls: ['./critt-desc-regime.component.scss']
})
export class CrittDescRegimeComponent implements OnInit {

  descRegimes: DescRegime[];

  constructor(private descRegimeService: DescRegimeService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    const sectCode = this.route.snapshot.queryParams['SECT'];
    const sousSectCode = this.route.snapshot.queryParams['SS'];
    const metierCode = this.route.snapshot.queryParams['codeMetier'];

    const DIPCode = this.route.snapshot.queryParams['DIP'];
    const DOMCode = this.route.snapshot.queryParams['DOM'];
    const COMCode = this.route.snapshot.queryParams['COM'];
    const DESCCode = this.route.snapshot.queryParams['DESC'];



    let paramsInString = "";
    paramsInString = paramsInString + "codeMetier="+metierCode+"&";
    paramsInString = paramsInString + "codeSS="+sousSectCode+"&";
    paramsInString = paramsInString + "codeS="+sectCode+"&";

    paramsInString = paramsInString + "codeDIP="+DIPCode+"&";
    paramsInString = paramsInString + "codeDOM="+DOMCode+"&";
    paramsInString = paramsInString + "codeCOM="+COMCode+"&";
    paramsInString = paramsInString + "codeDESC="+DESCCode;

    const params = new HttpParams({fromString: paramsInString});

    console.log('On cherche à obtenir les composantes sélectionnables');
    this.descRegimeService.find(params).subscribe(data => {
      this.descRegimes = data;
    });
  }

  onDescRegimeSelected(code: string){

    this.router.navigate(["/routing"], {
      relativeTo: this.route,
      queryParams: { DESC: code},
      queryParamsHandling: "merge"
    });

  }

}
