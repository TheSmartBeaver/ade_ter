import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrittDescRegimeComponent } from './critt-desc-regime.component';

describe('CrittDescRegimeComponent', () => {
  let component: CrittDescRegimeComponent;
  let fixture: ComponentFixture<CrittDescRegimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrittDescRegimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrittDescRegimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
