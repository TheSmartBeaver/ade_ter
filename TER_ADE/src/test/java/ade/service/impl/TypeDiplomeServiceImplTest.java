package ade.service.impl;

import ade.beans.TypeDiplome;
import ade.repository.TypeDiplomeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class TypeDiplomeServiceImplTest {

    @Mock
    private TypeDiplomeRepository typeDiplomeRepository;

    @InjectMocks
    @Spy
    private TypeDiplomeServiceImpl typeDiplomeService;

    @Test
    public void saveTest(){
        final TypeDiplome typeDiplome = TypeDiplome.builder().build();
        typeDiplomeService.save(typeDiplome);
        verify(typeDiplomeRepository).save(typeDiplome);

    }

    @Test
    public void saveAllTest(){
        final List<TypeDiplome> typeDiplomes = Collections.singletonList(TypeDiplome.builder().build());
        typeDiplomeService.saveAll(typeDiplomes);
        verify(typeDiplomeRepository).saveAll(typeDiplomes);
    }

    @Test
    public void findAllTest(){
        typeDiplomeService.findAll();
        verify(typeDiplomeRepository).findAll();
    }

    @Test
    public void findByCodeTest(){
        final String codeTypeDiplome = "codeTypeDiplome";
        typeDiplomeService.findByCode(codeTypeDiplome);
        verify(typeDiplomeRepository).findByCode(codeTypeDiplome);
    }
}
