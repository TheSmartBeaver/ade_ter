package ade.service.impl;

import ade.beans.Secteur;
import ade.beans.SousSecteur;
import ade.repository.SousSecteurRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class SousSecteurServiceImplTest {

    @Mock
    private SousSecteurRepository sousSecteurRepository;

    @InjectMocks
    @Spy
    private  SousSecteurServiceImpl sousSecteurService;

    @Test
    public void saveTest(){
        final SousSecteur sousSecteur = SousSecteur.builder().build();
        sousSecteurService.save(sousSecteur);
        verify(sousSecteurRepository).save(sousSecteur);
    }

    @Test
    public void saveAllTest(){
        final List<SousSecteur> sousSecteurs = Collections.singletonList(SousSecteur.builder().build());
        sousSecteurService.saveAll(sousSecteurs);
        verify(sousSecteurRepository).saveAll(sousSecteurs);
    }

    @Test
    public void deleteTest(){
        final String codeSousSecteur = "codeSousSecteur";
        final SousSecteur sousSecteur = SousSecteur.builder()
                .code(codeSousSecteur)
                .build();
        sousSecteurService.delete(codeSousSecteur);
        verify(sousSecteurRepository).deleteByCode(codeSousSecteur);
    }

    @Test
    public void updateTest(){
        final String codeSousSecteur = "codeSousSecteur";
        final SousSecteur sousSecteur = SousSecteur.builder()
                .code(codeSousSecteur)
                .nom("nouveau sous secteur")
                .description("description du nouveau sous secteur")
                .build();

        final SousSecteur ancienSousSecteur = SousSecteur.builder()
                .code(codeSousSecteur)
                .nom("ancien sous secteur")
                .description("description d'ancien sous secteur")
                .build();
        doReturn(ancienSousSecteur).when(sousSecteurService).findByCode(codeSousSecteur);
        doReturn(sousSecteur).when(sousSecteurRepository).save(any());

        sousSecteurService.update(sousSecteur);
        verify(sousSecteurService).findByCode(codeSousSecteur);

        final ArgumentCaptor<SousSecteur> sousSecteurArgumentCaptor = ArgumentCaptor.forClass(SousSecteur.class);
        verify(sousSecteurRepository).save(sousSecteurArgumentCaptor.capture());

        final SousSecteur capturedSecteur = sousSecteurArgumentCaptor.getValue();
        assertAll(
                ()-> assertEquals(ancienSousSecteur.getNom() , capturedSecteur.getNom() ,"expected value (Nom) doesn't math the actuel value"),
                ()-> assertEquals(ancienSousSecteur.getDescription() , capturedSecteur.getDescription(),"expected value (Description) doesn't math the actuel value")
        );

    }

    @Test
    public void findByCodeTest(){
        final String codeSousSecteur = "codeSousSecteur";
        final SousSecteur sousSecteur = SousSecteur.builder()
                .code(codeSousSecteur)
                .build();
        doReturn(sousSecteur).when(sousSecteurRepository).findByCode(codeSousSecteur);
        assertEquals(codeSousSecteur,sousSecteurService.findByCode(codeSousSecteur).getCode());
    }
}
