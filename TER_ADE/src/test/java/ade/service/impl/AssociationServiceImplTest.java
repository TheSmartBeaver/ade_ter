package ade.service.impl;

import ade.beans.Association;
import ade.beans.Secteur;
import ade.repository.AssociationRepository;
import ade.service.CritereService;
import ade.utils.QueryParams;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AssociationServiceImplTest {

    @Mock
    private AssociationRepository associationRepository;

    @Mock
    private CritereService critereService;

    @InjectMocks
    @Spy
    private AssociationServiceImpl associationService;

    @Test
    public void saveTest(){
        final Association association = Association.builder().build();
        associationService.save(association);
        verify(associationRepository).save(association);
    }

    @Test
    public void saveAllTest(){
        final List<Association> associations = Collections.singletonList(Association.builder().build());
        associationService.saveAll(associations);
        verify(associationRepository).saveAll(associations);
    }

    @Test
    public void deleteAllTest(){
        associationService.deleteAll();
        verify(associationRepository).deleteAll();
    }

    @Test
    public void findTest(){
        final QueryParams qp = QueryParams.builder().build();
        List<Association> associationsList = Collections.singletonList(Association.builder().build());
        doReturn(associationsList).when(associationRepository).findAll();
        associationService.find(qp);
        assertNotNull( associationService.find(qp));

    }

}
