package ade.service.impl;

import ade.beans.DescRegime;
import ade.repository.DescRegimeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class DescRegimeServiceImplTest {

    @Mock
    private DescRegimeRepository descRegimeRepository;

    @InjectMocks
    @Spy
    private DescRegimeServiceImpl descRegimeService;

    @Test
    public void findAllTest(){
        descRegimeService.findAll();
        verify(descRegimeRepository).findAll();
    }

    @Test
    public void saveAllTest(){
        final List<DescRegime> descRegimes = Collections.singletonList(DescRegime.builder().build());
        descRegimeService.saveAll(descRegimes);
        verify(descRegimeRepository).saveAll(descRegimes);
    }

    @Test
    public void findAllByCodeTest(){
        final String codeDescRegime = "codeDescRegimes";
        descRegimeService.findAllByCode(codeDescRegime);
        verify(descRegimeRepository).findAllByCode(codeDescRegime);
        assertNotNull(descRegimeService.findAllByCode(codeDescRegime));
    }

    @Test
    public void findByCodeTest(){
        final String codeDescRegime = "codeDescRegimes";
        final DescRegime descRegime = DescRegime.builder()
                .code(codeDescRegime)
                .build();
        doReturn(descRegime).when(descRegimeRepository).findByCode(codeDescRegime);
        descRegimeService.findByCode(codeDescRegime);
        assertNotNull(descRegimeService.findByCode(codeDescRegime));
    }

    @Test
    public void deleteAll(){
        descRegimeService.deleteAll();
        verify(descRegimeRepository).deleteAll();
    }




}
