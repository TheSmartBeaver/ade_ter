package ade.service.impl;

import ade.beans.Composante;
import ade.beans.DescRegime;
import ade.repository.ComposanteRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ComposanteServiceImplTest {

    @Mock
    private ComposanteRepository composanteRepository;

    @InjectMocks
    @Spy
    private ComposanteServiceImpl composanteService;


    @Test
    public void findAllTest(){
        composanteService.findAll();
        verify(composanteRepository).findAll();
    }

    @Test
    public void saveAllTest(){
        final List<Composante> composantes = Collections.singletonList(Composante.builder().build());
        composanteService.saveAll(composantes);
        verify(composanteRepository).saveAll(composantes);
    }

    @Test
    public void findAllByCodeTest(){
        final String codeComposante = "codeComposante";
        composanteService.findAllByCode(codeComposante);
        verify(composanteRepository).findAllByCode(codeComposante);
        assertNotNull(composanteService.findAllByCode(codeComposante));
    }


    @Test
    public void deleteAll(){
        composanteService.deleteAll();
        verify(composanteRepository).deleteAll();
    }

}
