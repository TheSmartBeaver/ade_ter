package ade.service.impl;

import ade.beans.Domaine;
import ade.repository.DomaineRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class DomaineServiceImplTest {

    @Mock
    private DomaineRepository domaineRepository;

    @InjectMocks
    @Spy
    private DomaineServiceImpl domaineService;

    @Test
    public void saveAllTest(){
        final List<Domaine> domaines = Collections.singletonList(Domaine.builder().build());
        domaineService.saveAll(domaines);
        verify(domaineRepository).saveAll(domaines);
    }

    @Test
    public void findAllTest(){
        domaineService.findAll();
        verify(domaineRepository).findAll();
    }

    @Test
    public void deleteAll(){
        domaineService.deleteAll();
        verify(domaineRepository).deleteAll();
    }

    @Test
    public void findAllByCodeTest(){
        final String codeDomaines = "codeDomaines";
        domaineService.findAllByCode(codeDomaines);
        verify(domaineRepository).findAllByCode(codeDomaines);
        assertNotNull(domaineService.findAllByCode(codeDomaines));
    }

}
