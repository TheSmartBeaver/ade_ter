package ade.service.impl;

import ade.beans.Formation;
import ade.beans.Secteur;
import ade.repository.FormationRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class FormationServiceImplTest {

    @Mock
    private FormationRepository formationRepository;

    @InjectMocks
    @Spy
    private FormationServiceImpl formationService;

    @Test
    public void saveTest(){
        final Formation formation = Formation.builder().build();
        formationService.save(formation);
        verify(formationRepository).save(formation);
    }

    @Test
    public void saveAllTest(){
        List<Formation> formations = Collections.singletonList(Formation.builder().build());
        formationService.saveAll(formations);
        verify(formationRepository).saveAll(formations);
    }

    @Test
    public void getUrlOfFormationOfCodeTest(){
        String codeFormation = "codeFormation";
        String url = "formation.com";
        Formation formation = Formation.builder()
                .code(codeFormation)
                .webComplet(url)
                .build();
        doReturn(formation).when(formationService).findByCode(codeFormation);
        assertEquals(url,formationService.getUrlOfFormationOfCode(codeFormation));
    }

    @Test
    public void findByCodeTest(){
        final String codeFormation = "codeFormation";
        final Formation formation = Formation.builder()
                .code(codeFormation)
                .build();
        doReturn(formation).when(formationRepository).findByCode(codeFormation);
        assertEquals(codeFormation,formationService.findByCode(codeFormation).getCode());
    }

    @Test
    public void updateTest(){
        final String codeFormation = "codeFormation";
        final Formation formation = Formation.builder()
                .code(codeFormation)
                .nom("nouveau formation")
                .description("nouveau description")
                .build();

        final Formation ancienFormation = Formation.builder()
                .code(codeFormation)
                .nom("ancien formation")
                .description("ancien description")
                .build();
        doReturn(ancienFormation).when(formationService).findByCode(codeFormation);
        doReturn(formation).when(formationRepository).save(any());

        formationService.update(formation);
        verify(formationService).findByCode(codeFormation);

        final ArgumentCaptor<Formation> formationArgumentCaptor = ArgumentCaptor.forClass(Formation.class);
        verify(formationRepository).save(formationArgumentCaptor.capture());

        final Formation capturedFormation = formationArgumentCaptor.getValue();
        assertAll(
                ()-> assertEquals(ancienFormation.getNom() , capturedFormation.getNom() ,"expected value (Nom) doesn't math the actuel value"),
                ()-> assertEquals(ancienFormation.getDescription() , capturedFormation.getDescription(),"expected value (Description) doesn't math the actuel value")
        );

    }

    @Test
    public void deleteTest(){
        final String codeFormation = "codeFormation";
        final Formation formation = Formation.builder()
                .code(codeFormation)
                .build();
        doReturn(formation).when(formationService).findByCode(codeFormation);
        formationService.delete(codeFormation);
        verify(formationRepository).deleteByCode(codeFormation);
        assertEquals(formation.getCode(),formationService.delete(codeFormation).getCode());
    }
}
