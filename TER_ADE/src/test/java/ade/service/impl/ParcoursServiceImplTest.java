package ade.service.impl;

import ade.beans.Parcours;
import ade.beans.Secteur;
import ade.repository.ParcoursRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ParcoursServiceImplTest {

    @Mock
    private ParcoursRepository parcoursRepository;

    @InjectMocks
    @Spy
    private ParcoursServiceImpl parcoursService;

    @Test
    public void saveTest(){
        Parcours parcours = Parcours.builder().build();
        parcoursService.save(parcours);
        verify(parcoursRepository).save(parcours);
    }

    @Test
    public void findParcoursByCodeTest(){
        final String codeParcours = "codeParcours";
        final Parcours parcours = Parcours.builder()
                .code(codeParcours)
                .build();
        doReturn(parcours).when(parcoursRepository).findByCode(codeParcours);
        assertEquals(codeParcours,parcoursService.findParcoursByCode(codeParcours).getCode());
    }

    @Test
    public void updateTest(){
        final String codeParcours = "codeParcours";
        final Parcours parcours = Parcours.builder()
                .code(codeParcours)
                .nom("nouveau parcours")
                .build();

        final Parcours ancienParcours = Parcours.builder()
                .code(codeParcours)
                .nom("ancien parcours")
                .build();

        doReturn(ancienParcours).when(parcoursService).findParcoursByCode(codeParcours);
        doReturn(parcours).when(parcoursRepository).save(any());

        parcoursService.update(parcours);
        verify(parcoursService).findParcoursByCode(codeParcours);

        final ArgumentCaptor<Parcours> parcoursArgumentCaptor = ArgumentCaptor.forClass(Parcours.class);
        verify(parcoursRepository).save(parcoursArgumentCaptor.capture());

        final Parcours capturedParcours = parcoursArgumentCaptor.getValue();
        assertAll(
                ()-> assertEquals(ancienParcours.getNom() , capturedParcours.getNom() ,"expected value (Nom) doesn't math the actuel value")
        );
    }

    @Test
    public void deleteTest(){
        final String codeParcours = "codeParcours";
        final Parcours parcours = Parcours.builder()
                .code(codeParcours)
                .build();
        doReturn(parcours).when(parcoursService).findParcoursByCode(codeParcours);
        parcoursService.delete(codeParcours);
        verify(parcoursRepository).deleteByCode(codeParcours);
        assertEquals(parcours.getCode(),parcoursService.delete(codeParcours).getCode());
    }

}
