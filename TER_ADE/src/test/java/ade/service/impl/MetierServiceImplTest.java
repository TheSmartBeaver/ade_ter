package ade.service.impl;

import ade.beans.Metier;
import ade.beans.Secteur;
import ade.repository.MetierRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class MetierServiceImplTest {

    @Mock
    private MetierRepository metierRepository;

    @InjectMocks
    @Spy
    private MetierServiceImpl metierService;

    @Test
    public void saveTest(){
        final Metier metier = Metier.builder().build();
        metierService.save(metier);
        verify(metierRepository).save(metier);
    }

    @Test
    public void saveAllTest(){
        final List<Metier> metiers = Collections.singletonList(Metier.builder().build());
        metierService.saveAll(metiers);
        verify(metierRepository).saveAll(metiers);
    }

    @Test
    public void findByCodeTest(){
        final String codeMetier = "codeMetier";
        final Metier metier = Metier.builder()
                .code(codeMetier)
                .build();

        doReturn(metier).when(metierRepository).findByCode(codeMetier);
        assertEquals(codeMetier,metierService.findByCode(codeMetier).getCode());
    }

    @Test
    public void updateTest(){
        final String codeMetier = "codeMetier";
        final Metier metier = Metier.builder()
                .code(codeMetier)
                .nom("nouveau metier")
                .description(" description du nouveau metier")
                .build();

        final Metier ancienMetier = Metier.builder()
                .code(codeMetier)
                .nom("ancien metier")
                .description(" description d'ancien metier")
                .build();

        doReturn(ancienMetier).when(metierService).findByCode(codeMetier);
        doReturn(metier).when(metierRepository).save(any());

        metierService.update(metier);
        verify(metierService).findByCode(codeMetier);

        final ArgumentCaptor<Metier> metierArgumentCaptor = ArgumentCaptor.forClass(Metier.class);
        verify(metierRepository).save(metierArgumentCaptor.capture());

        final Metier capturedMetier = metierArgumentCaptor.getValue();
        assertAll(
                ()-> assertEquals(ancienMetier.getNom() , capturedMetier.getNom() ,"expected value (Nom) doesn't math the actuel value"),
                ()-> assertEquals(ancienMetier.getDescription() , capturedMetier.getDescription(),"expected value (Description) doesn't math the actuel value")
        );

    }

    @Test
    public void deleteTest(){
        final String codeMetier = "codeMetier";
        final Metier metier = Metier.builder()
                .code(codeMetier)
                .build();

        doReturn(metier).when(metierService).findByCode(codeMetier);
        metierService.delete(codeMetier);
        verify(metierRepository).deleteByCode(codeMetier);
        assertEquals(metier.getCode(),metierService.delete(codeMetier).getCode());
    }
}
