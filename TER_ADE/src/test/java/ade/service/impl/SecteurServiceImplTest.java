package ade.service.impl;

import ade.beans.Secteur;
import ade.repository.SecteurRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class SecteurServiceImplTest {

    @Mock
    private SecteurRepository secteurRepository;

    @InjectMocks
    @Spy
    private SecteurServiceImpl secteurService;

    @Test
    public void findByCodeTest(){
        final String codeSecteur = "codeSecteur";
        final Secteur secteur = Secteur.builder()
                .code(codeSecteur)
                .build();
        doReturn(secteur).when(secteurRepository).findByCode(codeSecteur);
        assertEquals(codeSecteur,secteurService.findByCode(codeSecteur).getCode());
    }

    @Test
    public void saveTest(){
        final Secteur secteur = Secteur.builder().build();
        secteurService.save(secteur);
        verify(secteurRepository).save(secteur);
    }

    @Test
    public void saveAllTest(){
        final List<Secteur> secteurs = Collections.singletonList(Secteur.builder().build());
        secteurService.saveAll(secteurs);
        verify(secteurRepository).saveAll(secteurs);
    }

    @Test
    public void updateTest(){
        final String codeSecteur = "codeSecteur";
        final Secteur secteur = Secteur.builder()
                .code(codeSecteur)
                .nom("nouveau secteur")
                .description("description du nouveau secteur")
                .build();

        final Secteur ancienSecteur = Secteur.builder()
                .code(codeSecteur)
                .nom("ancien secteur")
                .description("description d'ancien secteur")
                .build();
        doReturn(ancienSecteur).when(secteurService).findByCode(codeSecteur);
        doReturn(secteur).when(secteurRepository).save(any());

        secteurService.update(secteur);
        verify(secteurService).findByCode(codeSecteur);

        final ArgumentCaptor<Secteur> secteurArgumentCaptor = ArgumentCaptor.forClass(Secteur.class);
        verify(secteurRepository).save(secteurArgumentCaptor.capture());

        final Secteur capturedSecteur = secteurArgumentCaptor.getValue();
        assertAll(
                ()-> assertEquals(ancienSecteur.getNom() , capturedSecteur.getNom() ,"expected value (Nom) doesn't math the actuel value"),
                ()-> assertEquals(ancienSecteur.getDescription() , capturedSecteur.getDescription(),"expected value (Description) doesn't math the actuel value")
        );

    }

    @Test
    public void findAllTest(){
        final List<Secteur> secteurs = Collections.singletonList(Secteur.builder().build());
        doReturn(secteurs).when(secteurRepository).findAll();
        secteurService.findAll();
        verify(secteurRepository).findAll();
        assertEquals(secteurs,secteurService.findAll());
    }

    @Test
    public void deleteTest(){
        final String codeSecteur = "codeSecteur";
        final Secteur secteur = Secteur.builder()
                .code(codeSecteur)
                .build();
        doReturn(secteur).when(secteurService).findByCode(codeSecteur);
        secteurService.delete(codeSecteur);
        verify(secteurRepository).deleteByCode(codeSecteur);
        assertEquals(secteur.getCode(),secteurService.delete(codeSecteur).getCode());
    }
}
