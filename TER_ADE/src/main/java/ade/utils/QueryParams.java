package ade.utils;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class QueryParams {
    private String codeS;
    private String codeSS;
    private String codeMetier;

    private String codeDOM;
    private String codeDIP;
    private String codeDESC;
    private String codeCOM;
}
