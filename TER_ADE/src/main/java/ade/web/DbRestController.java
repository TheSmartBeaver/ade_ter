package ade.web;

import ade.beans.*;
import ade.service.*;
import ade.utils.QueryParams;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Log4j2
@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class DbRestController {

    private final SecteurService secteurService;
    private final SousSecteurService sousSecteurService;
    private final FormationService formationService;
    private final MetierService metierService;
    private final ParcoursService parcoursService;
    private final ComposanteService composanteService;
    private final TypeDiplomeService typeDiplomeService;
    private final DescRegimeService descRegimeService;
    private final DomaineService domaineService;
    private final AssociationService associationService;


    /*Retourne un JSON contenant tous les secteurs*/
    @GetMapping("/secteursList")
    public HashSet<Secteur> getSecteurs(@RequestParam(value = "codeSS", defaultValue = "-1") String valueSS,
                                        @RequestParam(value = "codeS", defaultValue = "-1") String valueS,
                                        @RequestParam(value = "codeMetier", defaultValue = "-1") String valueMetier,
                                        @RequestParam(value = "codeDOM", defaultValue = "-1") String valueDOM,
                                        @RequestParam(value = "codeDIP", defaultValue = "-1") String valueDIP,
                                        @RequestParam(value = "codeCOM", defaultValue = "-1") String valueCOM,
                                        @RequestParam(value = "codeDESC", defaultValue = "-1") String valueDESC
                                    ) {
        System.err.println("On demande la liste des secteurs");
        System.err.println("params secteurs ::: "+valueMetier+" "+valueS+" "+valueSS);

        QueryParams qp = QueryParams.builder()
                .codeMetier(valueMetier)
                .codeS(valueS)
                .codeSS(valueSS)
                .codeCOM(valueCOM)
                .codeDESC(valueDESC)
                .codeDIP(valueDIP)
                .codeDOM(valueDOM)
                .build();

        HashSet<Secteur> secteurs = secteurService.find(qp);
        System.err.println("SSSSS "+secteurs);
        return secteurs;
    }

    @GetMapping("/metiersList")
    public HashSet<Metier> getMetiers(@RequestParam(value = "codeSS", defaultValue = "-1") String valueSS,
                                        @RequestParam(value = "codeS", defaultValue = "-1") String valueS,
                                        @RequestParam(value = "codeMetier", defaultValue = "-1") String valueMetier,
                                      @RequestParam(value = "codeDOM", defaultValue = "-1") String valueDOM,
                                      @RequestParam(value = "codeDIP", defaultValue = "-1") String valueDIP,
                                      @RequestParam(value = "codeCOM", defaultValue = "-1") String valueCOM,
                                      @RequestParam(value = "codeDESC", defaultValue = "-1") String valueDESC
    ) {
        System.err.println("On demande la liste des métiers");
        System.err.println("params métiers ::: "+valueMetier+" "+valueS+" "+valueSS);

        QueryParams qp = QueryParams.builder()
                .codeMetier(valueMetier)
                .codeS(valueS)
                .codeSS(valueSS)
                .codeCOM(valueCOM)
                .codeDESC(valueDESC)
                .codeDIP(valueDIP)
                .codeDOM(valueDOM)
                .build();

        HashSet<Metier> metiers = metierService.find(qp);
        System.err.println("MMMMMMM "+metiers);
        return metiers;
    }

    @GetMapping("/formationsList")
    public HashSet<Formation> getFormations(@RequestParam(value = "codeSS", defaultValue = "-1") String valueSS,
                                            @RequestParam(value = "codeS", defaultValue = "-1") String valueS,
                                            @RequestParam(value = "codeMetier", defaultValue = "-1") String valueMetier,
                                            @RequestParam(value = "codeDOM", defaultValue = "-1") String valueDOM,
                                            @RequestParam(value = "codeDIP", defaultValue = "-1") String valueDIP,
                                            @RequestParam(value = "codeCOM", defaultValue = "-1") String valueCOM,
                                            @RequestParam(value = "codeDESC", defaultValue = "-1") String valueDESC
    ) {
        System.err.println("On demande la liste des métiers");
        System.err.println("params métiers ::: "+valueMetier+" "+valueS+" "+valueSS+" "+valueDIP+" "+valueCOM+" "+valueDESC+" "+valueDOM);
        //System.exit(1);

        QueryParams qp = QueryParams.builder()
                .codeMetier(valueMetier)
                .codeS(valueS)
                .codeSS(valueSS)
                .codeCOM(valueCOM)
                .codeDESC(valueDESC)
                .codeDIP(valueDIP)
                .codeDOM(valueDOM)
                .build();

        HashSet<Formation> formations = formationService.find(qp);
        System.err.println("MMMMMMM "+formations);
        return formations;
    }

    @GetMapping("/composantes")
    public HashMap<String, String> getComposantes(@RequestParam(value = "codeSS", defaultValue = "-1") String valueSS,
                                                  @RequestParam(value = "codeS", defaultValue = "-1") String valueS,
                                                  @RequestParam(value = "codeMetier", defaultValue = "-1") String valueMetier,
                                                  @RequestParam(value = "codeDOM", defaultValue = "-1") String valueDOM,
                                                  @RequestParam(value = "codeDIP", defaultValue = "-1") String valueDIP,
                                                  @RequestParam(value = "codeCOM", defaultValue = "-1") String valueCOM,
                                                  @RequestParam(value = "codeDESC", defaultValue = "-1") String valueDESC
    ){
        System.err.println("Récup les composantes");
        HashMap composantes = new HashMap();

        QueryParams qp = QueryParams.builder()
                .codeMetier(valueMetier)
                .codeS(valueS)
                .codeSS(valueSS)
                .codeCOM(valueCOM)
                .codeDESC(valueDESC)
                .codeDIP(valueDIP)
                .codeDOM(valueDOM)
                .build();

        HashSet<Formation> formationsList = formationService.find(qp);

        for (Formation f : formationsList){
            if(f==null)
                continue;
            System.err.println("cccccc "+f);
            System.err.println("cc "+f.getCode()+f.getNom());
            for (WebLink composante : f.getComposante()){
                composantes.put(composante.getCode(),composante.getNom());
            }
        }

        System.err.println("COMPOSANTES RETENUS : "+composantes);
        return composantes;
    }

    @GetMapping("/typeDiplomes")
    public HashMap<String, String> getTypeDiplomes(@RequestParam(value = "codeSS", defaultValue = "-1") String valueSS,
                                                   @RequestParam(value = "codeS", defaultValue = "-1") String valueS,
                                                   @RequestParam(value = "codeMetier", defaultValue = "-1") String valueMetier,
                                                   @RequestParam(value = "codeDOM", defaultValue = "-1") String valueDOM,
                                                   @RequestParam(value = "codeDIP", defaultValue = "-1") String valueDIP,
                                                   @RequestParam(value = "codeCOM", defaultValue = "-1") String valueCOM,
                                                   @RequestParam(value = "codeDESC", defaultValue = "-1") String valueDESC) {
        System.err.println("Récup les types de diplome");
        HashMap diplomes = new HashMap();

        QueryParams qp = QueryParams.builder()
                .codeMetier(valueMetier)
                .codeS(valueS)
                .codeSS(valueSS)
                .codeCOM(valueCOM)
                .codeDESC(valueDESC)
                .codeDIP(valueDIP)
                .codeDOM(valueDOM)
                .build();

        HashSet<Formation> formationsList = formationService.find(qp);

        for (Formation f : formationsList){
            if(f==null)
                continue;
            System.err.println("cccccc diplome "+f);
            System.err.println("cc diplome "+f.getCode()+f.getNom());
            diplomes.put(f.getType(),typeDiplomeService.findByCode(f.getType()).getNom());
        }

        System.err.println("DIPLOMES RETENUS : "+diplomes);
        return diplomes;
    }

    @GetMapping("/descRegimes")
    public HashMap<String, String> getDescRegimes(@RequestParam(value = "codeSS", defaultValue = "-1") String valueSS,
                                                  @RequestParam(value = "codeS", defaultValue = "-1") String valueS,
                                                  @RequestParam(value = "codeMetier", defaultValue = "-1") String valueMetier,
                                                  @RequestParam(value = "codeDOM", defaultValue = "-1") String valueDOM,
                                                  @RequestParam(value = "codeDIP", defaultValue = "-1") String valueDIP,
                                                  @RequestParam(value = "codeCOM", defaultValue = "-1") String valueCOM,
                                                  @RequestParam(value = "codeDESC", defaultValue = "-1") String valueDESC) {
        System.err.println("Récup les descriptions de régimes");
        HashMap descRegimes = new HashMap();

        QueryParams qp = QueryParams.builder()
                .codeMetier(valueMetier)
                .codeS(valueS)
                .codeSS(valueSS)
                .codeCOM(valueCOM)
                .codeDESC(valueDESC)
                .codeDIP(valueDIP)
                .codeDOM(valueDOM)
                .build();

        HashSet<Formation> formationsList = formationService.find(qp);

        for (Formation f : formationsList){
            if(f==null)
                continue;
            System.err.println("cccccc descRegime "+f);
            System.err.println("cc descRegime"+f.getCode()+f.getNom());

            List<String> descRegimeCodeList = f.getCodeRegimes();
            System.err.println("DESC REG CODE "+descRegimeCodeList);
            if (descRegimeCodeList==null)
                continue;
            for (String descRegime : descRegimeCodeList){
                descRegimes.put(descRegime, descRegimeService.findByCode(descRegime).getNom());
            }
        }

        System.err.println("COMPOSANTES RETENUS : "+descRegimes);
        return descRegimes;

    }

    @GetMapping("/domaines")
    public HashMap<String, String> getDomaines(@RequestParam(value = "codeSS", defaultValue = "-1") String valueSS,
                                               @RequestParam(value = "codeS", defaultValue = "-1") String valueS,
                                               @RequestParam(value = "codeMetier", defaultValue = "-1") String valueMetier,
                                               @RequestParam(value = "codeDOM", defaultValue = "-1") String valueDOM,
                                               @RequestParam(value = "codeDIP", defaultValue = "-1") String valueDIP,
                                               @RequestParam(value = "codeCOM", defaultValue = "-1") String valueCOM,
                                               @RequestParam(value = "codeDESC", defaultValue = "-1") String valueDESC) {
        System.err.println("Récup les domaines");
        HashMap domaines = new HashMap();

        QueryParams qp = QueryParams.builder()
                .codeMetier(valueMetier)
                .codeS(valueS)
                .codeSS(valueSS)
                .codeCOM(valueCOM)
                .codeDESC(valueDESC)
                .codeDIP(valueDIP)
                .codeDOM(valueDOM)
                .build();

        HashSet<Formation> formationsList = formationService.find(qp);

        for (Formation f : formationsList){
            if(f==null)
                continue;
            System.err.println("cccccc domaine "+f);
            System.err.println("cc domaine "+f.getCode()+f.getNom());
            for (WebLink domaine : f.getDomaine()){
                domaines.put(domaine.getCode(),domaine.getNom());
            }
        }

        System.err.println("DOMAINES RETENUS : "+domaines);
        return domaines;
    }

    @GetMapping("/ssOfCode")
    public HashSet<SousSecteur> getSousSecteurOfCode(@RequestParam(value = "codeSS", defaultValue = "-1") String value) {
        HashSet ss = new HashSet();
        ss.add(sousSecteurService.findByCode(value));
        return ss;
    }

    @GetMapping("/metierOfCode")
    public HashSet<Metier> getMetierOfCode(@RequestParam(value = "codeMetier", defaultValue = "-1") String value) {
        HashSet metiers = new HashSet();
        metiers.add(metierService.findByCode(value));
        System.err.println("On demande les métiers de code "+value);
        return metiers;
    }

    @GetMapping("/urlFormation")
    public HashSet<String> getURLOfFormation(@RequestParam(value = "codeFormation", defaultValue = "-1") String value) {
        System.err.println("On demande url formation de code "+value);
        //return formationService.getUrlOfFormationOfCode(value);
        HashSet url = new HashSet();
        url.add(formationService.getUrlOfFormationOfCode(value));
        return url;
    }

    /*Retourne un JSON contenant tous les secteurs*/
    @GetMapping("/clearBDD")
    public void clearBDD() {
        secteurService.deleteAll();
        sousSecteurService.deleteAll();
        formationService.deleteAll();
        metierService.deleteAll();
        parcoursService.deleteAll();
        composanteService.deleteAll();
        typeDiplomeService.deleteAll();
        descRegimeService.deleteAll();
        domaineService.deleteAll();
        associationService.deleteAll();
        System.err.println("On nettoie la BDD");
    }
}
