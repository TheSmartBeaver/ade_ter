package ade.service;

import ade.beans.TypeDiplome;
import ade.utils.QueryParams;
import org.apache.poi.ss.formula.functions.T;

import java.util.HashSet;
import java.util.List;

public interface TypeDiplomeService {

    List<TypeDiplome> findAll();

    TypeDiplome findByCode(String code);

    List<TypeDiplome> saveAll(List<TypeDiplome> typeDiplomes);

    TypeDiplome save(TypeDiplome typeDiplome);

    void deleteAll();
}
