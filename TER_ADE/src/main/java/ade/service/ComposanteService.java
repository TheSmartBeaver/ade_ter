package ade.service;

import ade.beans.Composante;

import java.util.List;

public interface ComposanteService {

    List<Composante> findAllByCode(String code);

    List<Composante> findAll();

    List<Composante> saveAll(List<Composante> composantes);

    Composante save(Composante composante);

    void deleteAll();
}
