package ade.service;

import ade.beans.Secteur;
import ade.utils.QueryParams;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public interface SecteurService {

    Secteur findByCode(String code);

    Secteur save(Secteur secteur);

    List<Secteur> saveAll(List<Secteur> secteurs);

    Secteur update(Secteur secteur);

    List<Secteur> findAll();


    Secteur delete(String code);

    void deleteAll();

    HashSet<Secteur> find(QueryParams qp);
}
