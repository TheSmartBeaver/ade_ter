package ade.service;

import ade.beans.Formation;
import ade.beans.WebLink;
import ade.utils.QueryParams;

import java.util.HashSet;
import java.util.List;

public interface FormationService {

    List<Formation> saveAll(List<Formation> formations);

    String getUrlOfFormationOfCode(String code);

    Formation save(Formation formation);

    Formation findByCode(String code);


    Formation update(Formation formation);

    Formation delete(String code);

    void deleteAll();

    HashSet<Formation> find(QueryParams qp);

    boolean formationContainsDomaine(Formation f, String codeDomaine);

    boolean formationContainsComposante(Formation f, String codeComposante);
}
