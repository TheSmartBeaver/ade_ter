package ade.service.impl;

import ade.beans.*;
import ade.beans.Association;
import ade.beans.Secteur;
import ade.beans.WebLink;
import ade.repository.AssociationRepository;
import ade.repository.ParcoursRepository;
import ade.service.CritereService;
import ade.utils.QueryParams;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import ade.repository.FormationRepository;
import ade.service.FormationService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class FormationServiceImpl implements FormationService {

    private final FormationRepository formationRepository;
    private final AssociationRepository associationRepository;
    private final ParcoursRepository parcoursRepository;
    private final CritereService critereService;

    @Override
    public List<Formation> saveAll(List<Formation> formations) {
        return formationRepository.saveAll(formations);
    }

    @Override
    public Formation save(Formation formation) {
        return formationRepository.save(formation);
    }

    @Override
    public String getUrlOfFormationOfCode(String code){
        Formation formation = findByCode(code);
        return formation.getWebComplet();
    }

    @Override
    public Formation findByCode(String code) {
        Formation formation = formationRepository.findByCode(code);
        if(formation != null)
            return formation;
        else
        {
            Parcours p = parcoursRepository.findByCode(code);
            if (p==null)
                return null;
            //TODO: Que faire du code "TOUS" ???
            System.err.println("PARCOURS : "+p+" de code "+code);
            System.err.println("code de la formation du parcours "+p.getCodeFormation());
            formation = formationRepository.findByCode(p.getCodeFormation());
            return formation;
            }
    }

    @Override
    public Formation update(Formation formation) {
        Formation ancienFormation = findByCode(formation.getCode());
        ancienFormation.setNom(formation.getNom());
        ancienFormation.setComposante(formation.getComposante());
        ancienFormation.setDescription(formation.getDescription());
        ancienFormation.setDomaine(formation.getDomaine());
        ancienFormation.setExtra(formation.getExtra());
        ancienFormation.setInformations(formation.getInformations());
        ancienFormation.setParcours(formation.getParcours());
        ancienFormation.setType(formation.getType());
        ancienFormation.setVille(formation.getVille());
        ancienFormation.setWebComplet(formation.getWebComplet());
        ancienFormation.setWebCompletAnglais(formation.getWebCompletAnglais());
        return formationRepository.save(ancienFormation);
    }

    @Override
    public Formation delete(String code) {
        Formation formation = findByCode(code);
        formationRepository.deleteByCode(code);
        return formation;
    }

    @Override
    public void deleteAll(){
        formationRepository.deleteAll();
    }

    public boolean formationContainsDomaine(Formation f, String codeDomaine){
        if(f==null)
            return false;

        for(WebLink domaine : f.getDomaine()){
            if (domaine.getCode().equals(codeDomaine))
                return true;
        }
        return false;
    }

    public boolean formationContainsComposante(Formation f, String codeComposante){
        if(f==null)
            return false;

        for(WebLink composante : f.getComposante()){
            if (composante.getCode().equals(codeComposante))
                return true;
        }
        return false;
    }

    @Override
    public HashSet<Formation> find(QueryParams qp) {

        HashSet<Formation> formations = new HashSet<>();
        HashSet<String> formationsCodes = new HashSet<>();

        List<Association> assos;
        if(!qp.getCodeSS().equals("undefined")&&!qp.getCodeMetier().equals("undefined")) {
            System.err.println("Pas de crit SS et métier, youhou la recherche sera plus rapide ? " + qp.getCodeMetier() +" "+qp.getCodeSS());
            assos = associationRepository.findAllByCodeMetierAndCodeSousSecteur(qp.getCodeMetier(), qp.getCodeSS());
        }
        else if(!qp.getCodeSS().equals("undefined"))
            assos = associationRepository.findAllByCodeSousSecteur(qp.getCodeSS());
        else if(!qp.getCodeMetier().equals("undefined"))
            assos = associationRepository.findAllByCodeMetier(qp.getCodeMetier());
        else {
            assos = associationRepository.findAll();
        }
        System.err.println("Assos à analyser pour trouver les bonnes formations "+assos);

        for (Association as : assos){

            if(!critereService.checkIfAssociationRespectCritereParameters(as,qp))
                continue;

            formationsCodes.add(as.getCodeFormation());
        }
        System.err.println("formCODES "+formationsCodes);

        for(String m : formationsCodes) {
            formations.add(findByCode(m));
            System.err.println("Rajoute cette formation de code "+m);
        }
        System.err.println("formationS "+formations);

        return formations;
    }

}
