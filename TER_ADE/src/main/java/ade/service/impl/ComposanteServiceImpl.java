package ade.service.impl;

import ade.repository.ComposanteRepository;
import ade.beans.Composante;
import ade.service.ComposanteService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ComposanteServiceImpl implements ComposanteService {

    private final ComposanteRepository composanteRepository;

    @Override
    public List<Composante> findAllByCode(String code) {
        return composanteRepository.findAllByCode(code);
    }

    @Override
    public List<Composante> findAll() {
        return composanteRepository.findAll();
    }

    @Override
    public List<Composante> saveAll(List<Composante> composantes) {
        return composanteRepository.saveAll(composantes);
    }

    @Override
    public Composante save(Composante composante) {
        return composanteRepository.save(composante);
    }


    @Override
    public void deleteAll() {
        composanteRepository.deleteAll();
    }
}
