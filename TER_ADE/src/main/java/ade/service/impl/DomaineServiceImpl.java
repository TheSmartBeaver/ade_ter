package ade.service.impl;

import ade.beans.*;
import ade.repository.AssociationRepository;
import ade.repository.DomaineRepository;
import ade.service.DomaineService;
import ade.service.FormationService;
import ade.utils.QueryParams;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class DomaineServiceImpl implements DomaineService {

    private final DomaineRepository domaineRepository;
    private final AssociationRepository associationRepository;
    private final FormationService formationService;

    @Override
    public List<Domaine> findAll() {
        return domaineRepository.findAll();
    }

    @Override
    public List<Domaine> saveAll(List<Domaine> domaines) {
        return domaineRepository.saveAll(domaines);
    }

    @Override
    public List<Domaine> findAllByCode(String code){
        return domaineRepository.findAllByCode(code);
    }


    @Override
    public void deleteAll() {
        domaineRepository.deleteAll();


    }
}
