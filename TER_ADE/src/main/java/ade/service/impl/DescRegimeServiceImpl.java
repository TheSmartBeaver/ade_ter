package ade.service.impl;

import ade.beans.Composante;
import ade.repository.DescRegimeRepository;
import ade.beans.DescRegime;
import ade.service.DescRegimeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DescRegimeServiceImpl implements DescRegimeService {

    private final DescRegimeRepository descRegimeRepository;

    @Override
    public List<DescRegime> findAll() {
        return descRegimeRepository.findAll();
    }

    @Override
    public List<DescRegime> findAllByCode(String code) {
        return descRegimeRepository.findAllByCode(code);
    }

    @Override
    public DescRegime findByCode(String code) {
        return descRegimeRepository.findByCode(code);
    }

    @Override
    public List<DescRegime> saveAll(List<DescRegime> descRegimes) {
        return descRegimeRepository.saveAll(descRegimes);
    }

    @Override
    public void deleteAll() {
        descRegimeRepository.deleteAll();
    }

}
