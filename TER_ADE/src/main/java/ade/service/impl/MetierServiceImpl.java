package ade.service.impl;

import ade.beans.Association;
import ade.beans.Metier;
import ade.beans.Secteur;
import ade.repository.AssociationRepository;
import ade.repository.FormationRepository;
import ade.repository.MetierRepository;
import ade.service.CritereService;
import ade.service.MetierService;
import ade.utils.QueryParams;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class MetierServiceImpl implements MetierService {

    private final MetierRepository metierRepository;
    private final AssociationRepository associationRepository;
    private final CritereService critereService;

    @Override
    public Metier save(Metier metier) {
        return metierRepository.save(metier);
    }

    @Override
    public List<Metier> saveAll(List<Metier> metiers) {
        return metierRepository.saveAll(metiers);
    }

    @Override
    public Metier findByCode(String code) {
        return metierRepository.findByCode(code);
    }

    @Override
    public Metier update(Metier metier) {
        Metier ancienMetier = findByCode(metier.getCode());
        ancienMetier.setNom(metier.getNom());
        ancienMetier.setDescription(metier.getDescription());
        ancienMetier.setFormations(metier.getFormations());
        ancienMetier.setMotsClefs(metier.getMotsClefs());
        return metierRepository.save(ancienMetier);
    }

    @Override
    public Metier delete(String code) {
        Metier metier = findByCode(code);
        metierRepository.deleteByCode(code);
        return metier;
    }

    @Override
    public void deleteAll() {
        metierRepository.deleteAll();
    }

    @Override
    public HashSet<Metier> find(QueryParams qp) {

        Set<Secteur> secteurs = new HashSet<>();
        HashSet<Metier> metiers = new HashSet<>();
        HashSet<String> metiersCodes = new HashSet<>();

        List<Association> assos;
        if(!qp.getCodeSS().equals("undefined"))
            assos = associationRepository.findAllByCodeSousSecteur(qp.getCodeSS());
        else
            assos = associationRepository.findAll();

        System.err.println("Assos à analyser pour trouver les bons métiers "+assos);
        for (Association as : assos){
            if(!critereService.checkIfAssociationRespectCritereParameters(as,qp))
                continue;

            metiersCodes.add(as.getCodeMetier());

        }

        for(String m : metiersCodes)
            metiers.add(metierRepository.findByCode(m));
        return metiers;
    }
}
