package ade.service.impl;

import ade.beans.Formation;
import ade.beans.Parcours;
import ade.repository.ParcoursRepository;
import ade.service.ParcoursService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ParcoursServiceImpl implements ParcoursService {

    private final ParcoursRepository parcoursRepository;

    @Override
    public Parcours save(Parcours parcours) {
        return parcoursRepository.save(parcours);
    }

    @Override
    public Parcours findParcoursByCode(String code) {
        return parcoursRepository.findByCode(code);
    }

    @Override
    public Parcours update(Parcours parcours) {
        Parcours ancienParcours = findParcoursByCode(parcours.getCode());
        ancienParcours.setNom(parcours.getNom());
        ancienParcours.setDescription(parcours.getDescription());
        ancienParcours.setExtra(parcours.getExtra());
        ancienParcours.setVille(parcours.getVille());
        ancienParcours.setWebComplet(parcours.getWebComplet());
        ancienParcours.setWebAnglais(parcours.getWebAnglais());
        return parcoursRepository.save(ancienParcours);
    }

    @Override
    public Parcours delete(String code) {
        Parcours parcours = findParcoursByCode(code);
        parcoursRepository.deleteByCode(code);
        return parcours;
    }

    @Override
    public void deleteAll(){
        parcoursRepository.deleteAll();
    }

}
