package ade.service.impl;

import ade.beans.*;
import ade.repository.AssociationRepository;
import ade.repository.SecteurRepository;
import ade.repository.SousSecteurRepository;
import ade.beans.Association;
import ade.beans.Secteur;
import ade.repository.AssociationRepository;
import ade.repository.SecteurRepository;
import ade.service.CritereService;
import ade.service.SecteurService;
import ade.service.SousSecteurService;
import ade.utils.QueryParams;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class SecteurServiceImpl implements SecteurService {

    private final SecteurRepository secteurRepository;
    private final SousSecteurService sousSecteurService;
    private final AssociationRepository associationRepository;
    private final CritereService critereService;

    @Override
    public Secteur findByCode(String code) {
        return secteurRepository.findByCode(code);
    }

    @Override
    public Secteur save(Secteur secteur) {
        return secteurRepository.save(secteur);
    }

    @Override
    public List<Secteur> saveAll(List<Secteur> secteurs) {
        return secteurRepository.saveAll(secteurs);
    }

    @Override
    public Secteur update(Secteur secteur) {
        Secteur ancienSecteur = findByCode(secteur.getCode());
        ancienSecteur.setNom(secteur.getNom());
        ancienSecteur.setDescription(secteur.getDescription());
        ancienSecteur.setSousSecteurs(secteur.getSousSecteurs());
        return secteurRepository.save(ancienSecteur);
    }

    @Override
    public List<Secteur> findAll() {
        List<Secteur> secteurs = secteurRepository.findAll();
        return secteurs;
    }

    @Override
    public HashSet<Secteur> find(QueryParams qp) {

        /*Les lignes suivantes optimisent le temps de réponse*/
        if(qp.getCodeSS().equals("undefined")&&qp.getCodeS().equals("undefined")&&qp.getCodeMetier().equals("undefined")&&qp.getCodeDIP().equals("undefined")&&qp.getCodeDOM().equals("undefined")&&
                qp.getCodeCOM().equals("undefined")&&qp.getCodeDESC().equals("undefined")){
            HashSet<Secteur> secteurs = new HashSet<>();
            for (Secteur c : secteurRepository.findAll())
                secteurs.add(c);
            System.err.println("Aucun critère spécifié donc on retourne tous les secteurs");
            return secteurs;
        }

        if(qp.getCodeSS().equals("undefined")&&!qp.getCodeS().equals("undefined")&&qp.getCodeMetier().equals("undefined")&&qp.getCodeDIP().equals("undefined")&&qp.getCodeDOM().equals("undefined")&&
                qp.getCodeCOM().equals("undefined")&&qp.getCodeDESC().equals("undefined")){
            HashSet<Secteur> secteurs = new HashSet<>();
                secteurs.add(secteurRepository.findByCode(qp.getCodeS()));
            System.err.println("Juste critère secteur spécifié donc on retourne le secteur");
            return secteurs;
        }

        Set<String> sousSecteurs = new HashSet<>();

        List<Association> assos;
        if(!qp.getCodeSS().equals("undefined")&&!qp.getCodeMetier().equals("undefined")) {
            System.err.println("Pas de crit SS et métier, youhou la recherche sera plus rapide ?" + qp.getCodeMetier() +" "+qp.getCodeSS());
            assos = associationRepository.findAllByCodeMetierAndCodeSousSecteur(qp.getCodeMetier(), qp.getCodeSS());
        }
        else if(!qp.getCodeSS().equals("undefined"))
            assos = associationRepository.findAllByCodeSousSecteur(qp.getCodeSS());
        else if(!qp.getCodeMetier().equals("undefined"))
            assos = associationRepository.findAllByCodeMetier(qp.getCodeMetier());
        else
            assos = associationRepository.findAll();

        for (Association as : assos){
            if(!critereService.checkIfAssociationRespectCritereParameters(as,qp))
                continue;

            sousSecteurs.add(as.getCodeSousSecteur());
        }
        System.err.println("On a récup "+sousSecteurs.size()+" sous-secteurs respectant les critères");
        //System.exit(1);

        return sousSecteurService.getSecteursOfSS(sousSecteurs);
    }

    @Override
    public Secteur delete(String code) {
        Secteur secteur = findByCode(code);
        secteurRepository.deleteByCode(code);
        return secteur;
    }

    @Override
    public void deleteAll() {
        secteurRepository.deleteAll();
    }

}
