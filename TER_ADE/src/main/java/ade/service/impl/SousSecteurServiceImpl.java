package ade.service.impl;

import ade.beans.Secteur;
import ade.beans.SousSecteur;
import ade.repository.SecteurRepository;
import ade.repository.SousSecteurRepository;
import ade.service.SousSecteurService;
import ade.utils.QueryParams;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class SousSecteurServiceImpl implements SousSecteurService {

    private final SousSecteurRepository sousSecteurRepository;
    private final SecteurRepository secteurRepository;

    @Override
    public SousSecteur save(SousSecteur sousSecteur) {
        return sousSecteurRepository.save(sousSecteur);
    }

    @Override
    public List<SousSecteur> saveAll(List<SousSecteur> sousSecteurs) {
        return sousSecteurRepository.saveAll(sousSecteurs);
    }

    @Override
    public SousSecteur delete(String code) {
        SousSecteur sousSecteur = findByCode(code);
        sousSecteurRepository.deleteByCode(code);
        return sousSecteur;
    }

    @Override
    public SousSecteur update(SousSecteur sousSecteur) {
        SousSecteur ancienSousSecteur = findByCode(sousSecteur.getCode());
        ancienSousSecteur.setNom(sousSecteur.getNom());
        ancienSousSecteur.setDescription(sousSecteur.getDescription());
        ancienSousSecteur.setMetiers(sousSecteur.getMetiers());
        return sousSecteurRepository.save(sousSecteur);
    }

    @Override
    public SousSecteur findByCode(String code) {
        return sousSecteurRepository.findByCode(code);
    }

    @Override
    public void deleteAll() {
        sousSecteurRepository.deleteAll();
    }

    @Override
    public HashSet<Secteur> getSecteursOfSS(Set<String> ssCode){

        List<Secteur> secteurs = secteurRepository.findAll();
        HashSet<Secteur> sSet = new HashSet<>();
        for(Secteur s : secteurs) {
            for (String str : ssCode) {
                if (s.getSousSecteurs().containsKey(str)) {
                    /*s.getSousSecteurs().clear();
                    s.getSousSecteurs().put(str, sousSecteurRepository.findByCode(str).getNom());*/
                    sSet.add(s);
                }
            }

        }

        List<String> ssToRemove = new ArrayList<>();
        for(Secteur s : sSet) {
            System.err.println("SBUB "+s);
            for (Map.Entry e : s.getSousSecteurs().entrySet()) {
                System.err.println("iii "+s+" "+e);
                if(!ssCode.contains(e.getKey())) {
                    ssToRemove.add(e.getKey().toString());
                    //s.getSousSecteurs().remove(e.getKey().toString());
                    System.err.println("On a pas trouvé "+e.getKey());
                }
            }
            System.err.println("ooo"+s);
        }
        System.err.println("ssToRemove : "+ssToRemove);
        for(Secteur s : sSet) {
            for (String key : ssToRemove)
                s.getSousSecteurs().remove(key);
        }
        System.err.println("sSet : "+sSet);
        return sSet;
    }

}
