package ade.service.impl;

import ade.repository.TypeDiplomeRepository;
import ade.beans.TypeDiplome;
import ade.service.TypeDiplomeService;
import ade.utils.QueryParams;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TypeDiplomeServiceImpl implements TypeDiplomeService {

    private final TypeDiplomeRepository typeDiplomeRepository;

    @Override
    public List<TypeDiplome> findAll() {
        return typeDiplomeRepository.findAll();
    }

    @Override
    public TypeDiplome findByCode(String code) {
        return typeDiplomeRepository.findByCode(code);
    }

    @Override
    public List<TypeDiplome> saveAll(List<TypeDiplome> typeDiplomes) {
        return typeDiplomeRepository.saveAll(typeDiplomes);
    }

    @Override
    public TypeDiplome save(TypeDiplome typeDiplome) {
        return typeDiplomeRepository.save(typeDiplome);
    }

    @Override
    public void deleteAll() {
        typeDiplomeRepository.deleteAll();
    }
}
