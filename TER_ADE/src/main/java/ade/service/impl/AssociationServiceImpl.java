package ade.service.impl;

import ade.beans.Association;
import ade.beans.Formation;
import ade.beans.Secteur;
import ade.repository.AssociationRepository;
import ade.repository.FormationRepository;
import ade.service.AssociationService;
import ade.service.CritereService;
import ade.service.FormationService;
import ade.utils.QueryParams;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class AssociationServiceImpl implements AssociationService {

    private final AssociationRepository associationRepository;
    private final FormationService formationService;
    private final CritereService critereService;

    @Override
    public Association save(Association association) {
        return associationRepository.save(association);
    }

    @Override
    public List<Association> saveAll(List<Association> associations) {
        return associationRepository.saveAll(associations);
    }

    public void deleteAll(){
        associationRepository.deleteAll();
    }

    public HashSet<Association> find(QueryParams qp) {

        HashSet<Association> associations = new HashSet<>();

        List<Association> assos;
        if(qp.getCodeSS()!="undefined"&&qp.getCodeMetier()!="undefined") {
            System.err.println("Pas de crit SS et métier, youhou la recherche sera plus rapide ?");
            assos = associationRepository.findAllByCodeMetierAndCodeSousSecteur(qp.getCodeMetier(), qp.getCodeSS());
        }
        else if(qp.getCodeSS()!="undefined")
            assos = associationRepository.findAllByCodeSousSecteur(qp.getCodeSS());
        else if(qp.getCodeMetier()!="undefined")
            assos = associationRepository.findAllByCodeMetier(qp.getCodeMetier());
        else
            assos = associationRepository.findAll();

        for (Association as : assos){

            if(!critereService.checkIfAssociationRespectCritereParameters(as,qp))
                continue;

            associations.add(as);
        }
        return associations;
    }
}
