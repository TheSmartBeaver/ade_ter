package ade.service.impl;

import ade.beans.Association;
import ade.beans.Formation;
import ade.beans.Parcours;
import ade.beans.WebLink;
import ade.repository.FormationRepository;
import ade.repository.ParcoursRepository;
import ade.service.CritereService;
import ade.service.FormationService;
import ade.utils.QueryParams;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CritereServiceImpl implements CritereService {

    private final FormationRepository formationRepository;
    private final ParcoursRepository parcoursRepository;

    @Override
    public boolean checkIfAssociationRespectCritereParameters(Association as, QueryParams qp) {
        Formation formation = findByCode(as.getCodeFormation());
        if (formation == null) {
            System.err.println("La formation est null pour ce code " + as.getCodeFormation());
            if (as.getCodeFormation().equals("TOUS"))
                return false;
            System.exit(1);
        }

        if (qp.getCodeMetier().equals("undefined") || as.getCodeMetier().equals(qp.getCodeMetier())) {

        } else {
            System.out.println("critère métier pas respecté par association pour "+qp.getCodeMetier()+" "+as.getCodeMetier());
            return false;
        }
        if (qp.getCodeSS().equals("undefined") || as.getCodeSousSecteur().equals(qp.getCodeSS())) {
        } else {
            System.out.println("critère SS pas respecté par association pour "+qp.getCodeSS()+" "+as.getCodeSousSecteur());
            return false;
        }
        if (qp.getCodeDIP().equals("undefined") || formation.getType().equals(qp.getCodeDIP())) {
        } else {
            System.out.println("critère diplome pas respecté par association pour "+qp.getCodeDIP()+" "+formation.getType());
            return false;
        }
        if (qp.getCodeDESC().equals("undefined") || formation.getCodeRegimes().contains(qp.getCodeDESC())) {
        } else {
            System.out.println("critère régime pas respecté par association pour "+qp.getCodeDIP()+" "+formation.getCodeRegimes());
            return false;
        }
        if (qp.getCodeCOM().equals("undefined") || formationContainsComposante(formation, qp.getCodeCOM())) {
        } else {
            System.out.println("critère composante pas respecté par association pour "+qp.getCodeCOM()+" "+formation.getComposante());
            return false;
        }
        if (qp.getCodeDOM().equals("undefined") || formationContainsDomaine(formation, qp.getCodeDOM())) {
        } else {
            System.out.println("critère domaine pas respecté par association pour "+qp.getCodeDOM()+" "+formation.getComposante());
            return false;
        }
        return true;
    }

    public Formation findByCode(String code) {
        Formation formation = formationRepository.findByCode(code);
        if (formation != null)
            return formation;
        else {
            Parcours p = parcoursRepository.findByCode(code);
            if (p == null)
                return null;
            //TODO: Que faire du code "TOUS" ???
            System.err.println("PARCOURS : " + p + " de code " + code);
            System.err.println("code de la formation du parcours " + p.getCodeFormation());
            formation = formationRepository.findByCode(p.getCodeFormation());
            return formation;
        }
    }

    public boolean formationContainsDomaine(Formation f, String codeDomaine) {
        if (f == null)
            return false;

        for (WebLink domaine : f.getDomaine()) {
            if (domaine.getCode().equals(codeDomaine))
                return true;
        }
        return false;
    }

    public boolean formationContainsComposante(Formation f, String codeComposante) {
        if (f == null)
            return false;

        for (WebLink composante : f.getComposante()) {
            if (composante.getCode().equals(codeComposante))
                return true;
        }
        return false;
    }
}
