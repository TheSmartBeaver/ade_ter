package ade.service;


import ade.beans.Composante;
import ade.beans.DescRegime;

import java.util.List;

public interface DescRegimeService {

    List<DescRegime> findAll();

    List<DescRegime> findAllByCode(String code);

    List<DescRegime> saveAll(List<DescRegime> descRegimes);

    DescRegime findByCode(String code);

    void deleteAll();

}
