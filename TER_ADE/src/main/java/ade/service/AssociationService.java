package ade.service;

import ade.beans.Association;
import ade.utils.QueryParams;

import java.util.HashSet;
import java.util.List;

public interface AssociationService {

    Association save(Association association);

    List<Association> saveAll(List<Association> associations);

    void deleteAll();

    HashSet<Association> find(QueryParams qp);
}
