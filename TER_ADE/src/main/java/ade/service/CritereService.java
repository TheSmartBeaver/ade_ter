package ade.service;

import ade.beans.Association;
import ade.utils.QueryParams;

public interface CritereService {
    boolean checkIfAssociationRespectCritereParameters(Association as, QueryParams qp);
}
