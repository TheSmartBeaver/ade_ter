package ade.service;

import ade.beans.Metier;
import ade.utils.QueryParams;

import java.util.HashSet;
import java.util.List;

public interface MetierService {

    Metier save(Metier metier);

    List<Metier> saveAll(List<Metier> metiers);

    Metier findByCode(String code);

    Metier update(Metier metier);

    Metier delete(String code);

    void deleteAll();

    HashSet<Metier> find(QueryParams qp);
}
