package ade.service;

import ade.beans.Composante;
import ade.beans.Domaine;

import java.util.List;

public interface DomaineService {

    List<Domaine> findAll();

    List<Domaine> findAllByCode(String code);

    List<Domaine> saveAll(List<Domaine> domaines);

    void deleteAll();
}
