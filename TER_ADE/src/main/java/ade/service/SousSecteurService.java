package ade.service;

import ade.beans.Secteur;
import ade.beans.SousSecteur;
import ade.utils.QueryParams;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public interface SousSecteurService {

    SousSecteur save(SousSecteur sousSecteur);

    List<SousSecteur> saveAll(List<SousSecteur> sousSecteurs);

    SousSecteur delete(String code);

    SousSecteur update(SousSecteur sousSecteur);

    SousSecteur findByCode(String code);

    void deleteAll();

    HashSet<Secteur> getSecteursOfSS(Set<String> ssCode);
}
