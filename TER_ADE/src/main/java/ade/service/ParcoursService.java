package ade.service;

import ade.beans.Parcours;
import ade.utils.QueryParams;

import java.util.HashSet;

public interface ParcoursService {

    Parcours save(Parcours parcours);

    Parcours findParcoursByCode(String code);

    Parcours update(Parcours parcours);

    Parcours delete(String code);

    void deleteAll();
}
