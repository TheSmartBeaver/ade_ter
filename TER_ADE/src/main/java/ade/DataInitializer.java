package ade;

import ade.beans.*;
import ade.repository.FormationRepository;
import ade.repository.ParcoursRepository;
import ade.repository.SecteurRepository;
import ade.service.*;
import ade.xmlparser.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Component
@RequiredArgsConstructor
public class DataInitializer {

    private static final String OFFRE_FORMATION_FILE_PATH = "data/offre_de_formation.xml";
    private final static String FILE_NAME = "asso_metier_secteur.xlsx";


    private final FormationRepository formationRepository;
    private final FormationService formationService;
    private final ComposanteService composanteService;
    private final TypeDiplomeService typeDiplomeService;
    private final DomaineService domaineService;
    private final DescRegimeService descRegimeService;
    private final SecteurService secteurService;
    private final SousSecteurService sousSecteurService;
    private final MetierService metierService;
    private final AssociationService associationService;
    private final ParcoursRepository parcoursRepository;

    private final FileDataLoader fileDataLoader;
    private Index document;
    List<Secteur> secteurs;
    List<SousSecteur> sousSecteurs;
    List<Metier> metiers;
    List<Association> associations;

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        // on va verifier qu'un seule document si elle est remplie alors les autres sont aussi
        final long nombreElements = formationRepository.count();

        // on vérifie si la base de données est déja remplie
        //si la BD est vide on va la remplir sinon on va afficher une info dans le log
        if(nombreElements == 0){
            // XML
            log.info("Lecture du fichier xml");
            document = (Index) fileDataLoader.loadXmlObject(OFFRE_FORMATION_FILE_PATH);
            log.info("Fin de lecture du fichier xml");
            List<Formation> formations = new ArrayList<>();
            List<TypeDiplome> typeDiplomes = new ArrayList<>();
            List<Composante> composantes = new ArrayList<>();
            List<Domaine> domaines = new ArrayList<>();
            List<DescRegime> descRegimes = new ArrayList<>();

            for (Mention mention : document.getMentions()) {
                formations.add(mapToFormation(mention));
            }

            for (TypeDiplomeXml typeDiplomeXml : document.getTypeDiplomeXmls()) {
                typeDiplomes.add(mapToTypeDiplome(typeDiplomeXml));
            }

            for (ComposanteXml composanteXml : document.getComposanteXmls()) {
                composantes.add(mapToComposante(composanteXml));
            }

            for (DomaineXml domaineXml : document.getDomaineXmls()) {
                domaines.add(mapToDomaine(domaineXml));
            }

            for (DescRegimeXml descRegimeXml : document.getDescRegimeXmls()) {
                descRegimes.add(mapToDescRegime(descRegimeXml));
            }
            formationService.saveAll(formations);
            typeDiplomeService.saveAll(typeDiplomes);
            composanteService.saveAll(composantes);
            domaineService.saveAll(domaines);
            descRegimeService.saveAll(descRegimes);

            // EXCEL
            log.info("Lecture du fichier EXCEL");
            InputStream inputStream = readXslxFile();
            if (inputStream != null) {
                try {
                    XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
                    secteurs = readSecteur(workbook.getSheetAt(2));
                    sousSecteurs = readSousSecteur(workbook.getSheetAt(2));
                    metiers = readMetier(workbook.getSheetAt(1));
                    associations = readAssociation(workbook.getSheetAt(0));

                    secteurService.saveAll(secteurs);
                    sousSecteurService.saveAll(sousSecteurs);
                    metierService.saveAll(metiers);
                    associationService.saveAll(associations);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            log.info("Fin de lecture du fichier EXCEL");
        }else{
            log.info("la base de données est déja remplie ");
        }
    }

    DescRegime mapToDescRegime(DescRegimeXml descRegimeXml) {
        return DescRegime.builder()
                .code(descRegimeXml.getCode())
                .nom(descRegimeXml.getNom())
                .build();
    }

    Domaine mapToDomaine(DomaineXml domaineXml) {
        return Domaine.builder()
                .code(domaineXml.getCode())
                .nom(domaineXml.getNom())
                .web(domaineXml.getWeb())
                .build();
    }

    Composante mapToComposante(ComposanteXml composanteXml) {
        return Composante.builder()
                .code(composanteXml.getCode())
                .nom(composanteXml.getNom())
                .web(composanteXml.getWeb())
                .build();
    }

    TypeDiplome mapToTypeDiplome(TypeDiplomeXml typeDiplomeXml) {
        return TypeDiplome.builder()
                .code(typeDiplomeXml.getCode())
                .nom(typeDiplomeXml.getNom())
                .web(typeDiplomeXml.getWeb())
                .build();
    }

    Formation mapToFormation(Mention mention) {
        List<DomaineXml> domainesXml = new ArrayList<>();
        for(String domaineCode : mention.getDomaine())
            domainesXml.add(getDomainById(domaineCode));

        List<ComposanteXml> composantesXml = new ArrayList<>();
        for(String composanteCode : mention.getComposante())
            composantesXml.add(getComposanteById(composanteCode));

        List<WebLink> domaineWebLinkList = new ArrayList<>();
        for(DomaineXml d : domainesXml){
            domaineWebLinkList.add(WebLink.builder()
                    .code(d.getCode())
                    .nom(d.getNom())
                    .web(d.getWeb())
                    .build());
        }

        List<WebLink> composanteWebLinkList = new ArrayList<>();
        for(ComposanteXml d : composantesXml){
            composanteWebLinkList.add(WebLink.builder()
                    .code(d.getCode())
                    .nom(d.getNom())
                    .web(d.getWeb())
                    .build());
        }

        List<Parcours> parcoursList = getListParcours(mention);

        return Formation.builder()
                .code(mention.getCode())
                .type(mention.getType())
                .nom(mention.getNom())
                .webComplet(mention.getWebComplet())
                .webCompletAnglais(mention.getWebAnglais())
                .description(mention.getDescription().toString())
                .domaine(domaineWebLinkList)
                .composante(composanteWebLinkList)
                .extra(mention.getExtra())
                .informations(mention.getInformation())
                .codeRegimes(mention.getCodeRegime())
                .parcours(parcoursList)
                .ville(mention.getVille())
                .build();


    }

    DomaineXml getDomainById(String code) {
        return document.getDomaineXmls().stream().filter(domaineXml -> domaineXml.getCode().equals(code)).findFirst().orElse(null);
    }

    ComposanteXml getComposanteById(String code) {
        return document.getComposanteXmls().stream().filter(composante -> composante.getCode().equals(code)).findFirst().orElse(null);
    }

    List<Parcours> getListParcours(Mention mention) {
        if (mention.getParcours() == null) {
            return new ArrayList<>();
        }
        List<Parcours> parcoursList = new ArrayList<>();
        for (int i = 0; i < mention.getParcours().size(); i++) {
            Parcours parcours = Parcours.builder()
                    .code(mention.getParcours().get(i).getCode())
                    .nom(mention.getParcours().get(i).getNom())
                    .description(mention.getParcours().get(i).getDescription())
                    .webComplet(mention.getParcours().get(i).getWebComplet())
                    .webAnglais(mention.getParcours().get(i).getWebAnglais())
                    .extra(mention.getParcours().get(i).getExtra())
                    .ville(mention.getParcours().get(i).getVille())
                    .codeFormation(mention.getCode())
                    .build();
            parcoursRepository.save(parcours);
            parcoursList.add(parcours);
        }
        return parcoursList;
    }

    private List<Secteur> readSecteur(XSSFSheet worksheet) {
        final List<Secteur> secteurs = new ArrayList<>();
        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            XSSFRow row = worksheet.getRow(i);
            if (row.getCell(SecteurFields.CODE_SOUS_SECTEUR.ordinal()) == null) {
                Secteur secteur = Secteur.builder()
                        .code(row.getCell(SecteurFields.CODE_SECTEUR.ordinal()).toString())
                        .nom(row.getCell(SecteurFields.NOM.ordinal()).toString())
                        .description(row.getCell(SecteurFields.DESCRIPTION.ordinal()).toString())
                        .build();
                secteurs.add(secteur);
            }
        }
        return secteurs;
    }

    private List<SousSecteur> readSousSecteur(XSSFSheet worksheet) {
        final List<SousSecteur> sousSecteurs = new ArrayList<>();
        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            XSSFRow row = worksheet.getRow(i);
            if (row.getCell(SecteurFields.CODE_SOUS_SECTEUR.ordinal()) != null) {
                SousSecteur sousSecteur = SousSecteur.builder()
                        .code(row.getCell(SecteurFields.CODE_SOUS_SECTEUR.ordinal()).toString())
                        .nom(row.getCell(SecteurFields.NOM.ordinal()).toString())
                        .description(row.getCell(SecteurFields.DESCRIPTION.ordinal()).toString())
                        .build();
                sousSecteurs.add(sousSecteur);
                Secteur parent = findSecteurByCode(row.getCell(SecteurFields.CODE_SECTEUR.ordinal()).toString());
                if(parent != null){
                    parent.getSousSecteurs().put(row.getCell(SecteurFields.CODE_SOUS_SECTEUR.ordinal()).toString(), row.getCell(SecteurFields.NOM.ordinal()).toString());
                }
            }
        }
        return sousSecteurs;
    }

    private List<Metier> readMetier(XSSFSheet worksheet) {
        final List<Metier> metiers = new ArrayList<>();
        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            XSSFRow row = worksheet.getRow(i);
            Metier metier = Metier.builder()
                    .code(row.getCell(0).toString())
                    .nom(row.getCell(1).toString())
                    .motsClefs(row.getCell(2) != null ? row.getCell(2).toString() : null)
                    .description(row.getCell(3).toString())
                    .build();
            metiers.add(metier);
        }
        return metiers;
    }

    private List<Association> readAssociation(XSSFSheet worksheet){
        final List<Association> associations = new ArrayList<>();
        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            XSSFRow row = worksheet.getRow(i);
            Association association = Association.builder()
                    .ape(row.getCell(0).toString())
                    .codeFormation(row.getCell(3).toString())
                    .codeSousSecteur(row.getCell(5).toString())
                    .codeMetier(row.getCell(7).toString())
                    .build();
            associations.add(association);
            SousSecteur sousSecteur = findSousSecteurByCode(association.getCodeSousSecteur());
            if(sousSecteur != null){
                sousSecteur.getMetiers().put(row.getCell(7).toString(),row.getCell(8).toString());
            }
            Metier metier = findMetierByCode(association.getCodeMetier());
            if(metier != null){
                metier.getFormations().put(row.getCell(3).toString(),row.getCell(4).toString());
            }


        }
        return associations;
    }

    Secteur findSecteurByCode(String code) {
        return this.secteurs.stream().filter(secteur -> secteur.getCode().equals(code)).findFirst().orElse(null);
    }

    SousSecteur findSousSecteurByCode(String code){
        return this.sousSecteurs.stream().filter(sousSecteur -> sousSecteur.getCode().equals(code)).findFirst().orElse(null);
    }

    Metier findMetierByCode(String code){
        return this.metiers.stream().filter(metier -> metier.getCode().equals(code)).findFirst().orElse(null);
    }

    private InputStream readXslxFile() {
        try {
            Resource resource = new ClassPathResource(FILE_NAME);
            return resource.getInputStream();
        } catch (IOException e) {
            log.error("Impossible de lire le fichier {}", FILE_NAME, e);
            return null;
        }
    }

    enum SecteurFields {
        CODE_SECTEUR,
        CODE_SOUS_SECTEUR,
        NOM,
        DESCRIPTION;
    }
}
