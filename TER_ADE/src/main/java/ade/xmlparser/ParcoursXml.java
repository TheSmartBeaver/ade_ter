package ade.xmlparser;


import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class ParcoursXml {

    @XmlElement(name = "code")
    private String code;

    @XmlElement(name = "nom")
    private String nom;

    @XmlElement(name = "webComplet")
    private String webComplet;

    @XmlElement(name = "webAnglais")
    private String webAnglais;

    @XmlElement(name = "description")
    private Description description;

    @XmlElement(name = "extra")
    private String extra;

    @XmlElement(name = "ville")
    private List<String> ville;

}
