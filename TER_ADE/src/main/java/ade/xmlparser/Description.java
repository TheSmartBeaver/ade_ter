package ade.xmlparser;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;


@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class Description {

    @XmlAnyElement(DescriptionHandler.class)
    private String content;
}
