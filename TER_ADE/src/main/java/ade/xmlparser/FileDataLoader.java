package ade.xmlparser;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.introspect.AnnotationIntrospectorPair;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.Collections;
import java.util.List;

@Log4j2
@Component
public class FileDataLoader {

    private static final ObjectMapper XML_OBJECT_MAPPER;
//    private static final ObjectMapper CSV_OBJECT_MAPPER;

    static {
        JacksonXmlModule xmlModule = new JacksonXmlModule();
        xmlModule.setDefaultUseWrapper(false);

        XML_OBJECT_MAPPER = new XmlMapper(xmlModule);
        XML_OBJECT_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
        XML_OBJECT_MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        XML_OBJECT_MAPPER.setVisibility(VisibilityChecker.Std.defaultInstance().withFieldVisibility(JsonAutoDetect.Visibility.ANY));
        XML_OBJECT_MAPPER.registerModule(xmlModule);
        XML_OBJECT_MAPPER.enable(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME);
        XML_OBJECT_MAPPER.setAnnotationIntrospector(new AnnotationIntrospectorPair(new JacksonAnnotationIntrospector(),
                new JaxbAnnotationIntrospector(TypeFactory.defaultInstance())
        ));
    }
//
//    static {
//        CSV_OBJECT_MAPPER = new CsvMapper();
//    }

    public Object loadXmlObject(final String fileName) {
        try {
            JAXBContext jc = JAXBContext.newInstance(Index.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            final File file = new ClassPathResource(fileName).getFile();
            return unmarshaller.unmarshal(file);
        } catch (Exception e) {
            log.error("Error occurred while loading xml object from file " + fileName, e);
            return null;
        }
    }

    public <T> List<T> loadXmlObjectList(final Class<T> type, final String fileName) {
        try {
            final File file = new ClassPathResource(fileName).getFile();
            final MappingIterator<T> readValues =
                    XML_OBJECT_MAPPER.readerFor(type).readValues(file);
            return readValues.readAll();
        } catch (Exception e) {
            log.error("Error occurred while loading xml object list from file " + fileName, e);
            return Collections.emptyList();
        }
    }



}
