package ade.xmlparser;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class DomaineXml {

    @XmlElement(name = "code")
    private String code;

    @XmlElement(name = "nom")
    private String nom;

    @XmlElement(name = "web")
    private String web;
}
