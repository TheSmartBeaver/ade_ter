package ade.xmlparser;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class Mention {

    @XmlElement(name = "code")
    private String code;

    @XmlElement(name = "type")
    private String type;

    @XmlElement(name = "nom")
    private String nom;

    @XmlElement(name = "webComplet")
    private String webComplet;

    @XmlElement(name = "webAnglais")
    private String webAnglais;

    @XmlElement(name = "description")
    private Description description;


    @XmlElement(name = "domaine")
    private List<String> domaine;

    @XmlElement(name = "composante")
    private List<String> composante;

    @XmlElement(name = "extra")
    private String extra;

    @XmlElement(name = "information")
    private List<String> information;

    @XmlElement(name = "code_regime")
    private List<String> codeRegime;

    @XmlElement(name = "ville")
    private List<String> ville;

    @XmlElement(name = "parcours")
    private List<ParcoursXml> parcours;

}

