package ade.xmlparser;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "index")
@XmlAccessorType(XmlAccessType.FIELD)
public class Index {
    @JacksonXmlElementWrapper(useWrapping = false)
    @XmlElement(name = "mention")
    private List<Mention> mentions;

    @JacksonXmlElementWrapper(useWrapping = false)
    @XmlElement(name = "typeDiplome")
    private List<TypeDiplomeXml> typeDiplomeXmls;

    @JacksonXmlElementWrapper(useWrapping = false)
    @XmlElement(name = "composante")
    private List<ComposanteXml> composanteXmls;

    @JacksonXmlElementWrapper(useWrapping = false)
    @XmlElement(name = "domaine")
    private List<DomaineXml> domaineXmls;

    @JacksonXmlElementWrapper(useWrapping = false)
    @XmlElement(name = "desc_regime")
    private List<DescRegimeXml> descRegimeXmls;


}
