package ade.beans;

import ade.xmlparser.Description;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
@Getter
@Setter
@Builder
public class Parcours {

    private String codeFormation;

    private String code;

    private String nom;

    private String webComplet;

    private String webAnglais;

    private Description description;

    private String extra;

    private List<String> ville;
}
