package ade.beans;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class WebLink {

    private String code;

    private String nom;

    private String web;
}