package ade.beans;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.Map;

@Document
@Getter
@Setter
@Builder
public class Metier {

    @Id
    private String code;
    private String nom;
    private String description;
    private String motsClefs;
    @Builder.Default
    private Map<String, String> formations = new HashMap<>();
}
