package ade.beans;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
@Getter
@Setter
@Builder
public class Formation {

    @Id
    private String code;

    private String nom;

    private String type;

    private String webComplet;

    private String webCompletAnglais;

    private String description;

    private List<WebLink> domaine;

    private List<WebLink> composante;

    //TODO: Rajouter les code_regime en plus des balises "information"

    private List<String> informations;

    private List<String> codeRegimes;

    private String extra;

    private List<ade.beans.Parcours> parcours;

    private List<String> ville;




}

