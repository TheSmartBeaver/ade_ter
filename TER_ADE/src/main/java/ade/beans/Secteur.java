package ade.beans;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.Singular;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.Map;

@Document
@Getter
@Setter
@Builder
public class Secteur {

    @Id
    private String code;
    private String nom;
    private String description;

    @Builder.Default
    private Map<String, String> sousSecteurs = new HashMap<>();

}
