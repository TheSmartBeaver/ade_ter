package ade.beans;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
@Getter
@Setter
@Builder
public class Association {

    @Id
    private String code;

    private String ape;

    private String codeFormation;

    private String codeMetier;

    private String codeSousSecteur;

}
