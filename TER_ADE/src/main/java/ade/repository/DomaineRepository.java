package ade.repository;

import ade.beans.Domaine;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface DomaineRepository extends MongoRepository<Domaine,String> {
    List<Domaine> findAllByCode(String code);
}
