package ade.repository;

import ade.beans.Metier;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MetierRepository extends MongoRepository<Metier, String> {
    Metier findByCode(String code);
    Metier deleteByCode(String code);
}

