package ade.repository;

import ade.beans.Parcours;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ParcoursRepository extends MongoRepository<Parcours, String> {

    Parcours findByCode(String code);
    Parcours deleteByCode(String code);
}
