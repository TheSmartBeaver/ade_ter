package ade.repository;

import ade.beans.TypeDiplome;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TypeDiplomeRepository extends MongoRepository<TypeDiplome, String> {

    TypeDiplome findByCode(String code);

}
