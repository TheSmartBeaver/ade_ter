package ade.repository;

import ade.beans.DescRegime;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface DescRegimeRepository extends MongoRepository<DescRegime,String> {

    List<DescRegime> findAllByCode(String code);

    DescRegime findByCode(String code);

}
