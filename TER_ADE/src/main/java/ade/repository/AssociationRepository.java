package ade.repository;

import ade.beans.Association;
import ade.beans.Metier;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface AssociationRepository extends MongoRepository<Association, Integer> {
    Association findByCode(String code);
    List<Association> findAllByCodeMetierAndCodeSousSecteur(String codeMetier, String codeSousSecteur);
    List<Association> findAllByCodeMetier(String codeMetier);
    List<Association> findAllByCodeSousSecteur(String codeSousSecteur);
}
