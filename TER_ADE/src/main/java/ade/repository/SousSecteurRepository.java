package ade.repository;

import ade.beans.SousSecteur;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.HashSet;

public interface SousSecteurRepository extends MongoRepository<SousSecteur, String> {

    SousSecteur findByCode(String code);

    SousSecteur deleteByCode(String code);
}

