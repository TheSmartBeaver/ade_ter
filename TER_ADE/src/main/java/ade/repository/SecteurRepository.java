package ade.repository;

import ade.beans.Secteur;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SecteurRepository extends MongoRepository<Secteur, String> {

    Secteur findByCode(String code);

    Secteur deleteByCode(String code);


}

