package ade.repository;

import ade.beans.Composante;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ComposanteRepository extends MongoRepository<Composante, Integer> {

    List<Composante> findAllByCode(String code);
}
