package ade.repository;

import ade.beans.Formation;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FormationRepository extends MongoRepository<Formation, String> {
    Formation findByCode(String code);

    Formation deleteByCode(String code);
}
